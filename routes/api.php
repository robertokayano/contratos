<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//API Campos Transparência Index
Route::group([
    'namespace' => 'Api',
], function () {
    //API Campos Transparência Index
    Route::get('transparenciaorgaos', 'ApiTransparenciaController@orgaos');
    Route::get('transparenciaunidades', 'ApiTransparenciaController@unidades');
    Route::get('transparenciafornecedores', 'ApiTransparenciaController@fornecedores');
    Route::get('transparenciacontratos', 'ApiTransparenciaController@contratos');
});


Route::group([
    'namespace' => 'Api',
    'middleware' => 'throttleIp'
], function () {

    Route::group([
        'prefix' => 'comprasnet'
    ], function () {
        Route::post('/contratosempenhos', 'ComprasnetController@getContratosEmpenhosPorItens');
        Route::get('/contratos', 'ComprasnetController@getDadosContratosPorItem');
    });

    //API Consulta Contratos
    Route::group([
        'prefix' => 'contrato',
    ], function () {
        Route::get('/', 'ContratoController@contratoAtivoAll');
        Route::get('/ug/{unidade_codigo}', 'ContratoController@contratoAtivoPorUg');
        Route::get('/orgao/{orgao}', 'ContratoController@contratoAtivoPorOrgao');

        Route::get('/inativo/ug/{unidade_codigo}', 'ContratoController@contratoInativoPorUg');
        Route::get('/inativo/orgao/{orgao}', 'ContratoController@contratoInativoPorOrgao');

        Route::get('/{contrato_id}/historico', 'ContratoController@historicoPorContratoId');

        Route::get('/{contrato_id}/garantias', 'ContratoController@garantiasPorContratoId');
        Route::get('/{contrato_id}/itens', 'ContratoController@itensPorContratoId');
        Route::get('/{contrato_id}/prepostos', 'ContratoController@prepostosPorContratoId');
        Route::get('/{contrato_id}/responsaveis', 'ContratoController@responsaveisPorContratoId');
        Route::get('/{contrato_id}/despesas_acessorias', 'ContratoController@despesasAcessoriasPorContratoId');
        Route::get('/{contrato_id}/faturas', 'ContratoController@faturasPorContratoId');
        Route::get('/{contrato_id}/ocorrencias', 'ContratoController@ocorrenciasPorContratoId');
        Route::get('/{contrato_id}/terceirizados', 'ContratoController@terceirizadosPorContratoId');
        Route::get('/{contrato_id}/arquivos', 'ContratoController@arquivosPorContratoId');

        Route::get('/ugorigem/{codigo_uasg}/numeroano/{numeroano_contrato}', 'ContratoController@contratoUASGeContratoAno');

        Route::get('/{contrato_id}/empenhos', 'ContratoController@empenhosPorContratoId');
        Route::get('/empenhos', 'ContratoController@empenhosPorContratos');
        Route::get('/{contrato_id}/cronograma', 'ContratoController@cronogramaPorContratoId');
        Route::get('/orgaos', 'ContratoController@orgaosComContratosAtivos');
        Route::get('/unidades', 'ContratoController@unidadesComContratosAtivos');
    });
/*
        //API Teste PNCP
        Route::group([
            'prefix' => 'pncp',
        ], function () {

            Route::get('/envia', 'PNCP\EnviaPNCPController@enviaPNCP');
            Route::get('/enviaUnidade/{key}', 'PNCP\EnviaPNCPController@enviaUnidade');
            Route::get('/loginPNCP', 'PNCP\UsuarioController@login');

            Route::get('/consultarContrato/{cnpj}/{ano}/{sequencial}', 'PNCP\ContratoController@consultarContrato');
            Route::get('/retificarContrato/{id}/{cnpj}/{ano}/{sequencial}', 'PNCP\ContratoController@retificarContrato');
            Route::get('/excluirContrato/{cnpj}/{ano}/{sequencial}', 'PNCP\ContratoController@excluirContrato');
            Route::get('/inserirContrato/{id}', 'PNCP\TestaAPIController@insereContrato');
            Route::get('/inserirEmpenho/{id}', 'PNCP\TestaAPIController@insereEmpenho');

            Route::get('/consultarDocumentoTermoContrato/{cnpj}/{ano}/{sequencial}/{sequencialTermo}', 'PNCP\DocumentoTermoContratoController@consultarDocumentoTermoContrato');
            Route::get('/inserirDocumentoTermoContrato/{id}/{cnpj}/{ano}/{sequencial}/{sequencialTermo}', 'PNCP\DocumentoTermoContratoController@inserirDocumentoTermoContrato');
            Route::get('/baixarDocumentoTermoContrato/{cnpj}/{ano}/{sequencial}/{sequencialTermo}/{sequencialDoc}', 'PNCP\DocumentoTermoContratoController@baixarDocumentoTermosDeContrato');
            Route::get('/excluirDocumentoTermoContrato/{cnpj}/{ano}/{sequencial}/{sequencialTermo}/{sequencialDoc}', 'PNCP\DocumentoTermoContratoController@excluirDocumentoTermoContrato');

            Route::get('/consultarDocumentoContrato/{cnpj}/{ano}/{sequencial}', 'PNCP\DocumentoContratoController@consultarDocumentoContrato');
            Route::get('/inserirDocumentoContrato/{id}/{cnpj}/{ano}/{sequencial}', 'PNCP\DocumentoContratoController@inserirDocumentoContrato');
            Route::get('/baixarDocumentoContrato/{cnpj}/{ano}/{sequencial}/{sequencialDoc}', 'PNCP\DocumentoContratoController@baixarDocumentoContrato');
            Route::get('/excluirDocumentoContrato/{cnpj}/{ano}/{sequencial}/{sequencialDoc}', 'PNCP\DocumentoContratoController@excluirDocumentoContrato');

            Route::get('/insereUnidade/{cnpj}', 'PNCP\UnidadeController@insereUnidade');
            Route::get('/consultaUnidades/{cnpj}', 'PNCP\UnidadeController@consultaUnidades');
            Route::get('/buscaUnidade/{cnpj}/{codigo}', 'PNCP\UnidadeController@buscaUnidade');

            Route::get('/buscaUsuario/{usuario_identificador}', 'PNCP\UsuarioController@getUsuario');
            Route::get('/atualizaUsuario/{id}', 'PNCP\TestaAPIController@updateUsuario');
            Route::get('/insereUsuario', 'PNCP\TestaAPIController@insereUsuario');

            Route::get('/consultarTermoContrato/{cnpj}/{ano}/{sequencial}/{sequecialTermo}', 'PNCP\TermoContratoController@consultarTermoContrato');
            Route::get('/retificarTermoContrato/{id}/{cnpj}/{ano}/{sequencial}/{sequecialTermo}', 'PNCP\TermoContratoController@retificarTermoContrato');
            Route::get('/excluirTermoContrato/{cnpj}/{ano}/{sequencial}/{sequecialTermo}', 'PNCP\TermoContratoController@excluirTermoContrato');
            Route::get('/consultarTermosContrato/{cnpj}/{ano}/{sequencial}', 'PNCP\TermoContratoController@consultarTermosDeContrato');
            Route::get('/inserirTermoContrato/{id}/{cnpj}/{ano}/{sequencial}', 'PNCP\TermoContratoController@inserirTermoContrato');

            Route::get('/inserirOrgao/{id}', 'PNCP\OrgaoController@insereOrgao');
            Route::get('/consultaOrgao/{id}', 'PNCP\OrgaoController@consultaOrgao');

        });
*/
    Route::group([
        'prefix' => 'empenho',
    ], function () {
        Route::get('/ano/{ano}/ug/{unidade}', 'EmpenhoController@empenhosPorAnoUg');
        Route::get('/ug/{unidade}', 'EmpenhoController@empenhosPorUg');
//        Route::put('/sem/contrato/e/{empenho}/f/{fornecedor}/c/{contrato}', 'EmpenhoController@gravaContratoEmpenho');
    });

    Route::group([
        'prefix' => 'cronograma',
    ], function () {
        Route::get('/ug/{unidade}', 'ContratocronogramaController@cronogramaPorUg');
    });

});

Route::group([
    'namespace' => 'Api',
    'prefix' => 'v1',
], function () {
    Route::group([
        'middleware' => 'api',
        'prefix' => 'auth'
    ], function ($router) {
        Route::post('login', 'v1\UserJwtController@login');
        Route::post('logout', 'v1\UserJwtController@logout');
        Route::post('refresh', 'v1\UserJwtController@refresh');
        Route::get('me', 'v1\UserJwtController@me');
    });

    //consulta API com verificação do Token JWT e Perfil usuário Acesso API
    Route::group(['middleware' => ['jwt.verify']], function () {

        Route::group([
            'prefix' => 'comprasnet'
        ], function () {
            Route::post('/contratosempenhos', 'ComprasnetController@getContratosEmpenhosPorItens');
            Route::get('/contratos', 'ComprasnetController@getDadosContratosPorItem');
        });

        //API Consulta Contratos
        Route::group([
            'prefix' => 'contrato',
        ], function () {
            Route::get('/', 'ContratoController@contratoAtivoAll');
            Route::get('/ug/{unidade_codigo}', 'ContratoController@contratoAtivoPorUg');
            Route::get('/orgao/{orgao}', 'ContratoController@contratoAtivoPorOrgao');

            Route::get('/inativo/ug/{unidade_codigo}', 'ContratoController@contratoInativoPorUg');
            Route::get('/inativo/orgao/{orgao}', 'ContratoController@contratoInativoPorOrgao');

            Route::get('/{contrato_id}/historico', 'ContratoController@historicoPorContratoId');
            Route::get('/historicos', 'ContratoController@historicos');

            Route::get('/{contrato_id}/garantias', 'ContratoController@garantiasPorContratoId');
            Route::get('/garantias', 'ContratoController@garantias');
            Route::get('/{contrato_id}/itens', 'ContratoController@itensPorContratoId');
            Route::get('/itens', 'ContratoController@itens');
            Route::get('/{contrato_id}/prepostos', 'ContratoController@prepostosPorContratoId');
            Route::get('/prepostos', 'ContratoController@prepostos');
            Route::get('/{contrato_id}/responsaveis', 'ContratoController@responsaveisPorContratoId');
            Route::get('/responsaveis', 'ContratoController@responsaveis');
            Route::get('/{contrato_id}/despesas_acessorias', 'ContratoController@despesasAcessoriasPorContratoId');
            Route::get('/despesas_acessorias', 'ContratoController@despesasAcessorias');
            Route::get('/{contrato_id}/faturas', 'ContratoController@faturasPorContratoId');
            Route::get('/faturas', 'ContratoController@faturas');
            Route::get('/{contrato_id}/ocorrencias', 'ContratoController@ocorrenciasPorContratoId');
            Route::get('ocorrencias', 'ContratoController@ocorrencias');
            Route::get('/{contrato_id}/terceirizados', 'ContratoController@terceirizadosPorContratoId');
            Route::get('/terceirizados', 'ContratoController@terceirizados');
            Route::get('/{contrato_id}/arquivos', 'ContratoController@arquivosPorContratoId');
            Route::get('/arquivos', 'ContratoController@arquivos');
            Route::get('/publicacoes', 'ContratoController@publicacoes');

            Route::get('/ugorigem/{codigo_uasg}/numeroano/{numeroano_contrato}', 'ContratoController@contratoUASGeContratoAno');

            Route::get('/{contrato_id}/empenhos', 'ContratoController@empenhosPorContratoId');
            Route::get('/empenhos', 'ContratoController@empenhosPorContratos');
            Route::get('/empenhoporuasg/{codigo_unidade_emitente}/{numero_empenho}', 'MinutaEmpenhoController@minutasPorUasgPorNumeroEmpenho');

            Route::get('/{contrato_id}/cronograma', 'ContratoController@cronogramaPorContratoId');
            Route::get('/cronogramas', 'ContratoController@cronogramas');
            Route::get('/orgaos', 'ContratoController@orgaosComContratosAtivos');
            Route::get('/unidades', 'ContratoController@unidadesComContratosAtivos');
        });

        Route::group([
            'prefix' => 'empenho',
        ], function () {
            Route::get('/ano/{ano}/ug/{unidade}', 'EmpenhoController@empenhosPorAnoUg');
            Route::get('/ug/{unidade}', 'EmpenhoController@empenhosPorUg');
//        Route::put('/sem/contrato/e/{empenho}/f/{fornecedor}/c/{contrato}', 'EmpenhoController@gravaContratoEmpenho');
        });

        Route::group([
            'prefix' => 'cronograma',
        ], function () {
            Route::get('/ug/{unidade}', 'ContratocronogramaController@cronogramaPorUg');
        });

        Route::group([
            'middleware' => 'APIAntecipaGov',
            'prefix' => 'usuario',
        ], function () {
            Route::get('/ug/{unidade}', 'v1\UsuarioAntecipaGovController@usuariosPorUG');
            Route::get('/cpf/{cpf}', 'v1\UsuarioAntecipaGovController@usuarioPorCPF');
        });

    });
});


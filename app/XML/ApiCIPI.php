<?php


namespace App\XML;


class ApiCIPI
{
    protected $url_servico;
    protected $codigoOrgao;
    protected $sistema;
    protected $context;

    public function __construct()
    {
        $this->url_servico = config('api-cipi.url');
        $this->codigoOrgao = config('api-cipi.codigo_orgao');
        $this->sistema = config('api-cipi.sistema');
    }

    public function executaConsulta(string $tipo, array $dado, string $method = null)
    {
        $nome_funcao = 'consulta' . $tipo;

        return $this->$nome_funcao($dado);
    }

    private function submit(string $servico, array $params, string $method = null)
    {
        $retorno = '';

        $servico_especifico_login = $this->retornaServicoEspecifico('GERARTOKEN');
        $url_login = $this->url_servico . $servico_especifico_login;
        $token = $this->realizaLogin(config('api-cipi.sistema'),config('api-cipi.senha'),$url_login);

        if($token){
            $servico_especifico = $this->retornaServicoEspecifico($servico);
            $url = $this->url_servico . $servico_especifico;
            $arrToken = json_decode($token,true);
            $retorno = $this->executaEnvioPost($url, $params, $arrToken['token']);
            return $retorno;
        }else {
            return false;
        }


    }

    private function realizaLogin($usuario,$senha,$url)
    {

        $array_credenciais = [
            "senha" => $senha,
            'usuario' => $usuario
        ];

        $opts = [
            "http" => [
                "method" => "POST",
                "header" => [
                    'Content-Type: application/json',
                ],
                'content' => json_encode($array_credenciais)
            ],
            'ssl' => [
                'verify_peer' => false,
            ]
        ];

        $context = stream_context_create($opts);

        try {
            $data = file_get_contents($url, false, $context);
        } catch (\Exception $e) {
            $data = false;
        }

        return $data;
    }

    private function executaEnvioPost(string $url, array $params, $token)
    {

        $array_credenciais = [
            "credenciaisAcesso" => [
                "codigoOrgao" => $this->codigoOrgao,
                "cpf" => backpack_user()->cpf,
                "sistema" => $this->sistema
            ]
        ];

        $parametros = array_merge($array_credenciais, $params);

        $opts = [
            "http" => [
                "method" => "POST",
                "header" => [
                    'Content-Type: application/json',
                    'Authorization: Bearer '.$token,
                ],

                'content' => json_encode($parametros)
            ],
            'ssl' => [
                'verify_peer' => false,
            ]
        ];

        $context = stream_context_create($opts);


        try {
            $data = file_get_contents($url, false, $context);
        } catch (\Exception $e) {
            //var_dump($e);
            $data = false;
        }

        return $data;
    }

    private function retornaServicoEspecifico(string $servico)
    {
        $complemento_url = '';

        switch ($servico) {
            case 'VALIDACIPI':
                $complemento_url = 'protected/projetoinvestimento/validar';
                break;
            case 'GERARTOKEN':
                $complemento_url = 'public/sistema/gerar-token';
                break;
        }
        return $complemento_url;
    }

    private function consultaValidaCipi(array $dado, string $method = null)
    {
        return $this->submit('VALIDACIPI', $dado, $method);
    }

}

<?php

namespace App\Observers;

use App\Models\Codigoitem;
use App\Models\Contratoarquivo;
use App\Models\EnviaDadosPncp;
use Illuminate\Support\Facades\Log;

class ContratoarquivoObserver
{
    /**
     * Handle the contratoarquivo "created" event.
     *
     * @param  \App\Models\Contratoarquivo  $contratoarquivo
     * @return void
     */
    public function created(Contratoarquivo $contratoarquivo)
    {
        $this->preparaEnvioPNCP($contratoarquivo, 'INCARQ');
    }

    /**
     * Handle the contratoarquivo "updated" event.
     *
     * @param  \App\Models\Contratoarquivo  $contratoarquivo
     * @return void
     */
    public function updated(Contratoarquivo $contratoarquivo)
    {   
        $tipo = isset($contratoarquivo->sequencial_pncp) ? 'ATUARQ':'INCARQ';
        $this->preparaEnvioPNCP($contratoarquivo, $tipo);
    }

    /**
     * Handle the contratoarquivo "deleted" event.
     *
     * @param  \App\Models\Contratoarquivo  $contratoarquivo
     * @return void
     */
    public function deleted(Contratoarquivo $contratoarquivo)
    {
        if(isset($contratoarquivo->sequencial_pncp)){
            $this->preparaEnvioPNCP($contratoarquivo, 'DELARQ');
        }else if($contratoarquivo->envio_pncp_pendente !== null){
            $pncp = EnviaDadosPncp::where('pncpable_id',$contratoarquivo->contratohistorico_id)->first();
            $contratoarquivo->envio_pncp_pendente = null;
            $contratoarquivo->saveWithoutEvents();

            if($pncp->status->descres == 'ARQPEN'){

                if(!$this->verificaEnvioDeArquivosPendentes($contratoarquivo->contrato_id, $contratoarquivo->contratohistorico_id)){
                    $pncp->situacao = Codigoitem::whereHas('codigo', function ($q) {
                        $q->where('descricao', 'Situação Envia Dados PNCP');
                    })->where('descres', 'SUCESSO')->first()->id;
                    $pncp->save();
                }
            }
        }
    }

    /**
     * Handle the contratoarquivo "restored" event.
     *
     * @param  \App\Models\Contratoarquivo  $contratoarquivo
     * @return void
     */
    public function restored(Contratoarquivo $contratoarquivo)
    {
        //
    }

    /**
     * Handle the contratoarquivo "force deleted" event.
     *
     * @param  \App\Models\Contratoarquivo  $contratoarquivo
     * @return void
     */
    public function forceDeleted(Contratoarquivo $contratoarquivo)
    {
        if(isset($contratoarquivo->sequencial_pncp)){
            $this->preparaEnvioPNCP($contratoarquivo, 'DELARQ');
        }
    }

    private function preparaEnvioPNCP($contratoarquivo, $tipo){
        $tiposPNCP = ['Contrato', 'Termo Rescisão', 'Termo Aditivo', 'Termo Apostilamento'];
        if(in_array($contratoarquivo->getTipo(), $tiposPNCP)){
            $pncp = EnviaDadosPncp::where('pncpable_id',$contratoarquivo->contratohistorico_id)->where('pncpable_type', Contratohistorico::class)->first();
            if(isset($pncp)){
                //ARQPEN so entra quando o status é sucesso
                if($pncp->status->descres == 'SUCESSO'){
                    $pncp->situacao = Codigoitem::whereHas('codigo', function ($q) {
                        $q->where('descricao', 'Situação Envia Dados PNCP');
                    })->where('descres', 'ARQPEN')->first()->id;
                    $pncp->save();
                }
                $contratoarquivo = Contratoarquivo::withTrashed()->find($contratoarquivo->id);
                $contratoarquivo->envio_pncp_pendente = $tipo;
                $contratoarquivo->saveWithoutEvents();

            }
        }
    }

    private function verificaEnvioDeArquivosPendentes($id,$historico_id){
        return Contratoarquivo::withTrashed()->where([
            ['contrato_id', '=',$id],
            ['contratohistorico_id', '=',$historico_id]
        ])->whereNotNull('envio_pncp_pendente')->count() > 0? true:false;
        
    }

}


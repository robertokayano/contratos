<?php

namespace App\Observers;

use App\Http\Controllers\Publicacao\DiarioOficialClass;
use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\DiarioOficial;
use App\Http\Traits\Formatador;
use App\Models\BackpackUser;
use App\Models\CalendarEvent;
use App\Models\Codigoitem;
use App\Models\Contrato;
use App\Models\Contratocronograma;
use App\Models\Contratohistorico;
use App\Models\ContratoHistoricoMinutaEmpenho;
use App\Models\ContratoPublicacoes;
use DateTime;
use Alert;
use App\Models\Compra;
use App\Models\EnviaDadosPncp;
use Illuminate\Http\Request;
use Redirect;
use Route;
use Doctrine\DBAL\Schema\AbstractAsset;
use Exception;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ContratohistoricoObserve
{

    use BuscaCodigoItens;
    use Formatador;
    use DiarioOficial;

    private $request;

    public function __construct(Contratocronograma $contratocronograma, Request $request)
    {
        $this->contratocronograma = $contratocronograma;
        $this->request = $request;
    }

    /**
     * Handle the contratohistorico "created" event.
     *
     * @param Contratohistorico $contratohistorico
     * @return void
     */
    public function created(Contratohistorico $contratohistorico)
    {
        /**
         * vincula o amparo_legal ao contratohistorico
         */
        $contratohistorico->vincularAmparoLegalContratoHistorico($this->request);

        $historico = Contratohistorico::where('contrato_id', $contratohistorico->contrato_id)
            ->orderBy('data_assinatura', 'ASC')
            ->get();

        $this->contratocronograma->inserirCronogramaFromHistorico($contratohistorico);
        $this->atualizaContrato($historico);
        $this->createEventCalendar($contratohistorico);

        $tipoEmpenho = $this->retornaIdCodigoItem('Tipo de Contrato', 'Empenho');
        $tipoOutros = $this->retornaIdCodigoItem('Tipo de Contrato', 'Outros');
        //$tipoOutros = $this->retornaIdCodigoItem('Tipo de Contrato', 'Outros');

        $contratohistoricoBase = Contratohistorico::where('contrato_id', $contratohistorico->contrato_id)->oldest()->first();
        //Prepara o Historico para ser enviado para o PNCP
        //$tiposPNCP = ['Contrato', 'Termo Aditivo', 'Termo de Apostilamento', 'Termo de Rescisão'];
        $tiposNegadosPNCP = ['Empenho','Credenciamento', 'Outros'];
        if($contratohistoricoBase->possuiAmparo("LEI 14.133/2021") && 
        !in_array($contratohistorico->tipo->descricao, $tiposNegadosPNCP) && 
        $this->verificaCompraPncp($contratohistoricoBase) &&
        intval(explode('/', $contratohistoricoBase->licitacao_numero)[1]) >= 2021 &&
        (!$contratohistoricoBase->unidade->sigilo)){
            $this->preparaEnvioPNCP($contratohistorico->id, $contratohistorico->contrato_id,$contratohistorico->tipo->descricao, false);
        }

        if ($contratohistorico->tipo_id != $tipoEmpenho && $contratohistorico->tipo_id != $tipoOutros) {

            if($contratohistorico->publicado){
                $this->executaAtualizacaoViaJob($contratohistorico);
                return true;
            }

            if ($contratohistorico->publicado != true) {
                $this->criaNovaPublicacao($contratohistorico, $this->removeMascaraCPF(backpack_user()->cpf),true);
            }

        }

    }

    /**
     * Handle the contratohistorico "updated" event.
     *
     * @param Contratohistorico $contratohistorico
     * @return void
     */
    public function updated(Contratohistorico $contratohistorico)
    {

       $historico = Contratohistorico::where('contrato_id', $contratohistorico->contrato_id)
            ->orderBy('data_assinatura', 'ASC')
            ->get();
        $cronograma = Contratocronograma::where('contrato_id', $contratohistorico->contrato_id)
            ->delete();

        $this->contratocronograma->atualizaCronogramaFromHistorico($historico);
        $this->atualizaContrato($historico);
        $this->atualizaMinutasContrato($contratohistorico);
        $this->createEventCalendar($contratohistorico);
        
        $contratohistoricoBase = Contratohistorico::where('contrato_id', $contratohistorico->contrato_id)->oldest()->first();
        //Prepara o Historico para ser atualizado no PNCP
        //$tiposPNCP = ['Contrato', 'Termo Aditivo', 'Termo de Apostilamento', 'Termo de Rescisão'];
        Log::info("=======Atualizando Contrato: ".$contratohistorico->numero." - id:".$contratohistorico->contrato_id."=========");
        $tiposNegadosPNCP = ['Empenho', 'Credenciamento', 'Outros'];
        Log::info("Tem 14.133: ".$contratohistoricoBase->possuiAmparo("LEI 14.133/2021"));
        Log::info("É um tipo valido: (".$contratohistorico->tipo->descricao.") - ". !in_array($contratohistorico->tipo->descricao, $tiposNegadosPNCP));
        Log::info("Palpite>>>>>>>>>>: (".$contratohistoricoBase->tipo->descricao.") - ". !in_array($contratohistoricoBase->tipo->descricao, $tiposNegadosPNCP));
        if($contratohistoricoBase->possuiAmparo("LEI 14.133/2021") && 
        !in_array($contratohistorico->tipo->descricao, $tiposNegadosPNCP) &&
        (!$contratohistoricoBase->unidade->sigilo)){
            Log::info("Preparando atualização para o PNCP");
            $this->preparaEnvioPNCP($contratohistorico->id, $contratohistorico->contrato_id,$contratohistorico->tipo->descricao, true);
        }else{
            Log::info("Verificando se deve ser excluído");
           $this->verificaPNCP($contratohistorico->contrato_id, $contratohistorico->id);
        }

        //-------------------------------------------JOB-----------------------------------------------------------
        if ($contratohistorico->publicado && ($contratohistorico->publicacao->count() == 0)) {
            $this->executaAtualizacaoViaJob($contratohistorico);
            return true;
        }
        //-------------------------------------------------------------------------------------------------------------
        if (!is_null($contratohistorico->publicacao) && ($contratohistorico->publicado != true)) {
            if($this->booCampoPublicacaoAlterado($contratohistorico)) {
                $this->trataAtualizacaoPublicacoes($contratohistorico);
            }
        }

    }


    private function trataAtualizacaoPublicacoes($contratohistorico)
    {
        $sisg = (isset($contratohistorico->unidade->sisg)) ? $contratohistorico->unidade->sisg : '';

        $cpf = $this->removeMascaraCPF(backpack_user()->cpf);

        if(($contratohistorico->publicacao->count() == 0)){
            $this->criaNovaPublicacao($contratohistorico,$cpf,true);
            return true;
        }

        if(($contratohistorico->publicacao->count() == 1)){
            $publicacao = ContratoPublicacoes::where('contratohistorico_id',$contratohistorico->id)->first();
            $this->verificaStatusPublicacao($publicacao,$contratohistorico,$sisg,$cpf);
            return true;
        }

        if(($contratohistorico->publicacao->count() > 1)){
            $publicacao = ContratoPublicacoes::where('contratohistorico_id',$contratohistorico->id)->latest()->first();
            $this->verificaStatusPublicacao($publicacao,$contratohistorico,$sisg,$cpf);
            return true;
        }

    }


    public function verificaStatusPublicacao($publicacao,$contratohistorico,$sisg,$cpf)
    {

        $importado = $this->verificaPublicacaoImportada($publicacao,$contratohistorico,$sisg,$cpf);

        if(!$importado) {

            switch ($publicacao->status_publicacao_id) {
                case $this->retornaIdCodigoItem('Situacao Publicacao', 'PUBLICADO'):
                    $this->criaRetificacao($contratohistorico, $sisg,$cpf);
                    break;
                case $this->retornaIdCodigoItem('Situacao Publicacao', 'A PUBLICAR'):
                case $this->retornaIdCodigoItem('Situacao Publicacao', 'TRANSFERIDO PARA IMPRENSA'):
                    $this->verificaStatusOnline($publicacao, $contratohistorico, $sisg,$cpf);
                    break;
                default;
                    //todo verficar porque não redireciona.
                    return redirect('/gescon/contrato/' . $contratohistorico->contrato_id . '/publicacao');
            }
        }

    }


    public function verificaPublicacaoImportada($publicacao,$contratohistorico,$sisg,$cpf)
    {
        $retorno = false;

        $publicado = $this->retornaIdCodigoItem('Situacao Publicacao', 'PUBLICADO');

        if (($publicacao->status == "Importado") && ($publicacao->status_publicacao_id == $publicado)) {

            $this->criaRetificacao($contratohistorico,$sisg,$cpf);

            $retorno = true;
        }
        return $retorno;
    }


    private function executaAtualizacaoViaJob($contratohistorico)
    {
        $publicado_id = $this->retornaIdTipoPublicado();

        $publicacao = ContratoPublicacoes::Create(
            [
                'contratohistorico_id' => $contratohistorico->id,
                'status_publicacao_id' => $publicado_id,
                'data_publicacao' => $contratohistorico->data_publicacao,
                'texto_dou' => '',
                'status' => 'Importado',
                'tipo_pagamento_id' => $this->retornaIdCodigoItem('Forma Pagamento', 'Isento'),
                'motivo_isencao' => 0
            ]
        );
        return $publicacao;
    }

    public function deleting(Contratohistorico $contratohistorico)
    {
        try{
            $contratohistoricoBase = Contratohistorico::where('contrato_id', $contratohistorico->contrato_id)->oldest()->first();
            $tiposNegadosPNCP = ['Empenho', 'Credenciamento', 'Outros'];
            if($contratohistoricoBase->possuiAmparo("LEI 14.133/2021") && !in_array($contratohistorico->tipo->descricao, $tiposNegadosPNCP)){
                $this->preparaExclusaoPNCP($contratohistorico->contrato_id, $contratohistorico->id);
            }
        }catch(Exception $q){   
            Log::error($q);
        }
    }

    /**
     * Handle the contratohistorico "deleted" event.
     *
     * @param Contratohistorico $contratohistorico
     * @return void
     */
    public function deleted(Contratohistorico $contratohistorico)
    {
        $historico = Contratohistorico::where('contrato_id', '=', $contratohistorico->contrato_id)
            ->orderBy('data_assinatura', 'ASC')
            ->get();

        $cronograma = Contratocronograma::where('contrato_id', $contratohistorico->contrato_id)
            ->delete();

//        $contratohistorico->cronograma()->delete();
        $this->contratocronograma->atualizaCronogramaFromHistorico($historico);
        $this->atualizaContrato($historico);

    }

    /**
     * Handle the contratohistorico "restored" event.
     *
     * @param Contratohistorico $contratohistorico
     * @return void
     */
    public function restored(Contratohistorico $contratohistorico)
    {
        //
    }

    /**
     * Handle the contratohistorico "force deleted" event.
     *
     * @param Contratohistorico $contratohistorico
     * @return void
     */
    public function forceDeleted(Contratohistorico $contratohistorico)
    {
        //
    }

    private function atualizaContrato($contratohistorico)
    {
        foreach ($contratohistorico as $h) {
            $contrato_id = $h->contrato_id;
            $arrayhistorico = $h->toArray();

            $tipo = Codigoitem::find($arrayhistorico['tipo_id']);

            if ($tipo instanceof Codigoitem) {
                $array = $this->retornaArrayContratoHistorico($tipo, $arrayhistorico, $contrato_id);

                $contrato = new Contrato();
                $contrato->atualizaContratoFromHistorico($contrato_id, $array);
            }
        }
    }

    public function retornaArrayContratoHistorico(Codigoitem $tipo, array $arrayhistorico, $contrato_id)
    {
        switch ($tipo->descricao) {
            case 'Termo de Rescisão':
                return $this->retornaArrayRescisao($arrayhistorico);
                break;
            case 'Termo Aditivo':
                return $this->retornaArrayAditivo($arrayhistorico, $contrato_id);
                break;
            case 'Termo de Apostilamento':
                return $this->retornaArrayApostilamento($arrayhistorico);
                break;
            default:
                return $this->retornaArrayDefault($arrayhistorico);
        }
    }

    public function retornaArrayAditivo(array $arrayhistorico, $contrato_id)
    {

        $novo_valor = $arrayhistorico['valor_global'];

        if ($arrayhistorico['supressao'] == 'S') {
            $contrato = Contrato::find($contrato_id);
            $novo_valor = $contrato->valor_global - $novo_valor;
        }

        $arrayAditivo = [
            'fornecedor_id' => $arrayhistorico['fornecedor_id'],
            'unidade_id' => $arrayhistorico['unidade_id'],
            'info_complementar' => $arrayhistorico['info_complementar'],
//            'vigencia_inicio' => $arrayhistorico['vigencia_inicio'],
            'vigencia_fim' => $arrayhistorico['vigencia_fim'],
            'valor_global' => $novo_valor,
            'num_parcelas' => $arrayhistorico['num_parcelas'],
            'valor_parcela' => $arrayhistorico['valor_parcela'],
            'publicado' =>  $arrayhistorico['publicado'],
        ];
        (isset($arrayhistorico['situacao'])) ? $arrayAditivo['situacao'] = $arrayhistorico['situacao'] : "";
        return $arrayAditivo;
    }

    public function retornaArrayApostilamento(array $arrayhistorico)
    {
        $arrayApostilamento = [
            'fornecedor_id' => $arrayhistorico['fornecedor_id'],
            'unidade_id' => $arrayhistorico['unidade_id'],
            'valor_global' => $arrayhistorico['valor_global'],
            'num_parcelas' => $arrayhistorico['num_parcelas'],
            'valor_parcela' => $arrayhistorico['valor_parcela'],
            'publicado' =>  $arrayhistorico['publicado'],
        ];
        (isset($arrayhistorico['situacao'])) ? $arrayApostilamento['situacao'] = $arrayhistorico['situacao'] : "";
        return $arrayApostilamento;
    }

    public function retornaArrayRescisao(array $arrayhistorico)
    {
        $arrayRescisao = [
            'vigencia_fim' => $arrayhistorico['vigencia_fim'],
            'situacao' => $arrayhistorico['situacao'],
            'publicado' =>  $arrayhistorico['publicado'],
        ];
        return $arrayRescisao;
    }

    public function retornaArrayDefault(array $arrayhistorico)
    {
        unset($arrayhistorico['id']);
        unset($arrayhistorico['contrato_id']);
        unset($arrayhistorico['observacao']);
        unset($arrayhistorico['created_at']);
        unset($arrayhistorico['updated_at']);
        unset($arrayhistorico['retroativo']);
        unset($arrayhistorico['retroativo_mesref_de']);
        unset($arrayhistorico['retroativo_anoref_de']);
        unset($arrayhistorico['retroativo_mesref_ate']);
        unset($arrayhistorico['retroativo_anoref_ate']);
        unset($arrayhistorico['retroativo_vencimento']);
        unset($arrayhistorico['retroativo_valor']);
        unset($arrayhistorico['retroativo_soma_subtrai']);


        $arrayDefault = array_filter($arrayhistorico, function ($a) {
            return trim($a) !== "";
        });

        if (isset($arrayhistorico['situacao'])) {
            $arrayDefault['situacao'] = $arrayhistorico['situacao'];
        }

        return $arrayDefault;
    }

    public function createEventCalendar(Contratohistorico $contratohistorico)
    {
        $contrato = Contrato::find($contratohistorico->contrato_id);

        $fornecedor = $contrato->fornecedor->cpf_cnpj_idgener . ' - ' . $contrato->fornecedor->nome;
        $ug = $contrato->unidade->codigo . ' - ' . $contrato->unidade->nomeresumido;

        $tituloinicio = 'Início Vigência Contrato: ' . $contrato->numero . ' Fornecedor: ' . $fornecedor . ' da UG: ' . $ug;
        $titulofim = 'Fim Vigência Contrato: ' . $contrato->numero . ' Fornecedor: ' . $fornecedor . ' da UG: ' . $ug;

        $events = [
            [
                'title' => $tituloinicio,
                'start_date' => new DateTime($contrato->vigencia_inicio),
                'end_date' => new DateTime($contrato->vigencia_inicio),
                'unidade_id' => $contrato->unidade_id
            ],
            [
                'title' => $titulofim,
                'start_date' => new DateTime($contrato->vigencia_fim),
                'end_date' => new DateTime($contrato->vigencia_fim),
                'unidade_id' => $contrato->unidade_id
            ]

        ];

        foreach ($events as $e) {
            $calendario = new CalendarEvent();
            $calendario->insertEvents($e);
        }

        return $calendario;
    }

    private function getSituacao($sisg, $data = null,$create = false)
    {

        $situacao = Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', 'Situacao Publicacao');
        })
            ->select('codigoitens.id');

        if($create) {
            $data = Carbon::createFromFormat('Y-m-d', $data);
            if ($data->lte(Carbon::now())) {
                return $situacao->where('descricao', 'PUBLICADO')->first();
            }
        }

        if ($sisg) {
            return $situacao->where('descricao', 'A PUBLICAR')->first();
        }
        return $situacao->where('descricao', 'INFORMADO')->first();
    }

    private function retornaIdTipoPublicado()
    {
        return Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', 'Situacao Publicacao');
        })->where('descricao', 'PUBLICADO')->first()->id;
    }

    private function verificaStatusOnline($publicacao,$contratohistorico,$sisg,$cpf)
    {
        $diarioOficial = new DiarioOficialClass();
        $diarioOficial->atualizaStatusPublicacao($publicacao);

        (!is_null($publicacao->materia_id))
            ? $this->statusTransferidoParaImprensa($publicacao, $cpf, $contratohistorico, $sisg)
            : $this->statusAPublicar($publicacao,$cpf,$contratohistorico,$sisg);
    }

    private function statusTransferidoParaImprensa($publicacao,$cpf,$contratohistorico,$sisg)
    {

        $devolvido = $this->retornaIdCodigoItem('Situacao Publicacao', 'DEVOLVIDO PELA IMPRENSA');
        $sustada = $this->retornaIdCodigoItem('Situacao Publicacao', 'MATERIA SUSTADA');
        $publicado = $this->retornaIdCodigoItem('Situacao Publicacao', 'PUBLICADO');
        $transferido = $this->retornaIdCodigoItem('Situacao Publicacao', 'TRANSFERIDO PARA IMPRENSA');


        if ($publicacao->status_publicacao_id == $transferido) {
            $diarioOficial = new DiarioOficialClass();
            $retorno = $diarioOficial->sustaMateriaPublicacao($publicacao);

            if ($retorno->out->validaSustacao == "OK") {

                $publicacao->status = 'MATERIA SUSTADA';
                $publicacao->status_publicacao_id = $sustada;
                $publicacao->save();

                $statusPublicacao = ContratoPublicacoes::where('contratohistorico_id', $contratohistorico->id)
                    ->where('status_publicacao_id', $publicado)->first();

                //se houver alguma publicação com status publicado para esse instrumento CRIA RETIFICAÇÃO senão CRIANOVAPUBLICACAO
                (!is_null($statusPublicacao)) ? $this->criaRetificacao($contratohistorico, $sisg,$cpf)
                    : $this->criaNovaPublicacao($contratohistorico,$cpf);

            } else {
                $publicacao->status = 'ERRO AO TENTAR SUSTAR MATERIA';
                $publicacao->log = $retorno->out->validaSustacao;
                $publicacao->status_publicacao_id = $devolvido;
                $publicacao->save();
            }
        }
    }


    private function statusAPublicar($publicacao,$cpf,$contratohistorico,$sisg)
    {
        $publicado = $this->retornaIdCodigoItem('Situacao Publicacao', 'PUBLICADO');
        $statusPublicacao = ContratoPublicacoes::where('contratohistorico_id',$contratohistorico->id)
                ->where('status_publicacao_id',$publicado)->first();

        (!is_null($statusPublicacao)) ? $this->criaRetificacao($contratohistorico,$sisg,$cpf) : $this->criaNovaPublicacao($contratohistorico,$cpf);

    }



    private function atualizaMinutasContrato($contratohistorico)
    {
        // tipos que são permitidos manipular as minutas de empenho do contrato
        $tiposPermitidos = Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', '=', 'Tipo de Contrato');
        })
            ->where('descricao', '<>', 'Termo Aditivo')
            ->where('descricao', '<>', 'Termo de Apostilamento')
            ->where('descricao', '<>', 'Termo de Rescisão')
            ->orderBy('descricao')
            ->pluck('id')
            ->toArray();

        if (in_array($contratohistorico->tipo_id, $tiposPermitidos)) {
            $contrato = Contrato::find($contratohistorico->contrato_id);
            $contrato->minutasempenho()->detach();

            //todas minutas que serão vinculadas
            $arrContratoHistoricoMinutaEmpenho = ContratoHistoricoMinutaEmpenho::where('contrato_historico_id','=', $contratohistorico->id)->get();

            // vincula os empenhos ao contrato
            foreach ($arrContratoHistoricoMinutaEmpenho as $contratoHistoricoMinutaEmpenho) {
                $contrato->minutasempenho()->attach($contratoHistoricoMinutaEmpenho->minuta_empenho_id);
            }
        }
    }

    private function preparaEnvioPNCP($contratohistoricoId, $contratoId, $tipo, $atualizando){
        $pncp = EnviaDadosPncp::where([
            ['contrato_id', '=', $contratoId],
            ['pncpable_id', '=', $contratohistoricoId],
        ])->first();
        //dd("A variavel de controle do PNCP já existe?");
        if(isset($pncp)){
            Log::info("Variável de controle do PNCP FOI encontrada");
                //dd("ja foi para o PNCP?");
                if(isset($pncp->sequencialPNCP)){
                    /*$ultimaModificacaoEnviada = (isset($pncp->json_enviado_alteracao)) ? $pncp->json_enviado_alteracao : $pncp->json_enviado_inclusao;
                    $modificacaoAtual = ($tipo == 'Contrato') ? 
                        (Contratohistorico::find($contratohistoricoId))->serializaPNCP($atualizando) :
                        (Contratohistorico::find($contratohistoricoId))->serializaTermoPNCP($atualizando);
                    if($ultimaModificacaoEnviada !== json_encode($modificacaoAtual)){*/
                        $pncp->situacao = Codigoitem::whereHas('codigo', function ($q) {
                            $q->where('descricao', 'Situação Envia Dados PNCP');
                        })->where('descres', 'RETPEN')->first()->id;
                        $pncp->save();
                        Log::info("====================situação RETPEN definida, aguardando rotina=====================");
                    //}
                }else{
                    Log::info("====================nenhuma atitute tomada, pois o contrato ainda não foi enviado, aguardando rotina=====================");
                }
        }else{
            Log::info("Variável de controle do PNCP não foi encontrada, criando uma");
            EnviaDadosPncp::create([
                'pncpable_type' => ContratoHistorico::class,
                'contrato_id' => $contratoId,
                'situacao' => Codigoitem::whereHas('codigo', function ($q) {
                    $q->where('descricao', 'Situação Envia Dados PNCP');
                })->where('descres', 'INCPEN')->first()->id,
                'tipo_contrato' => $tipo,
                'pncpable_id' => $contratohistoricoId
            ]);
            Log::info("====================situação INCPEN definida, aguardando rotina=====================");
        }

    }

    private function preparaExclusaoPNCP($contratoId, $contratohistoricoId){
        $pncp = EnviaDadosPncp::where([
            ['contrato_id', '=', $contratoId],
            ['pncpable_id', '=', $contratohistoricoId],
        ])->first();
        if(isset($pncp)){
            if(isset($pncp->sequencialPNCP)){
                $pncp->situacao = Codigoitem::whereHas('codigo', function ($q) {
                    $q->where('descricao', 'Situação Envia Dados PNCP');
                })->where('descres', 'EXCPEN')->first()->id;
                $pncp->save();
                Log::info("====================situação EXCPEN definida, aguardando rotina=====================");
            }else{
                $pncp->delete();
            }
        }

    }

    private function verificaCompraPncp($contrato_historico){
        Log::info('unidade_origem_id: '. $contrato_historico->unidadecompra_id);
        Log::info('unidade_subrrogada_id: '. ($contrato_historico->unidade_id != $contrato_historico->unidadecompra_id) ? $contrato_historico->unidade_id : null);
        Log::info('modalidade_id: '. $contrato_historico->modalidade_id);
        Log::info('numero_ano: '. $contrato_historico->licitacao_numero);
        return Compra::where([
            ['unidade_origem_id', '=', $contrato_historico->unidadecompra_id],
            ['unidade_subrrogada_id', '=', ($contrato_historico->unidade_id != $contrato_historico->unidadecompra_id) ? $contrato_historico->unidade_id : null],
            ['modalidade_id', '=', $contrato_historico->modalidade_id],
            ['numero_ano', '=', $contrato_historico->licitacao_numero],
            ['lei', '=', 'LEI14133'],
        ])->count() > 0? true:false;

    }

    //Caso o usuário tenha inserido o amparo 14133 no cadastro e depois removeu
    private function verificaPNCP($contratoId, $contratohistoricoId){
        $pncp = EnviaDadosPncp::where([
            ['contrato_id', '=', $contratoId],
            ['pncpable_id', '=', $contratohistoricoId],
        ])->first();
        if(!isset($pncp)){
            Log::info("Variável de controle do PNCP não foi encontrada, nenhuma tarefa será realizada");
            return;
        }else{
            Log::info("Variável de controle do PNCP FOI encontrada");
            //Se tem sequencial, é por que ja está no PNCP
            if(isset($pncp->sequencialPNCP)){
                Log::info("O sequencial foi preenchido, preparando exclusão no PNCP");
                $this->preparaExclusaoPNCP($contratoId, $contratohistoricoId);
            }else{
                Log::info("O sequencial não foi preenchido, a variavel de controle será excluida");
                Log::info("====================Excluido sem EXCPEN=====================");
                $pncp->delete();
            }
        }
    }

}

<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class AmparoLegalRestricao extends Model
{
    use CrudTrait;
    use LogsActivity;
    // use SoftDeletes;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected static $logFillable = true;
    protected static $logName = 'amparo_legal_restricoes';

    protected $table = 'amparo_legal_restricoes';

    protected $fillable = [
        'amparo_legal_id',
        'tipo_restricao_id',
        'codigo_restricao',
        'regra'
    ];



    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getCodigoRestricao()
    {
        return $this->codigoItem->descricao;
    }

    public function labelRestricao()
    {
        return $this->amparoLegalRestricaoCodigo ? $this->amparoLegalRestricaoCodigo->nome : null;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function amparosLegais()
    {
        return $this->belongsTo(AmparoLegal::class, 'amparo_legal_id');
    }

    public function codigoItem()
    {
        return $this->belongsTo(Codigoitem::class, 'tipo_restricao_id');
    }


    public function amparoLegalRestricaoCodigo()
    {
        return $this->belongsTo(AmparoLegalRestricaoCodigo::class, 'codigo_restricao');
    }
}

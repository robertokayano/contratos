<?php

namespace App\Models;

use App\Http\Traits\Formatador;
use App\Models\Codigoitem;
use App\Models\Contratoarquivo;
use App\Models\Contratohistorico;
use App\Models\MinutaEmpenho;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class EnviaDadosPncp extends Model
{
    use CrudTrait;
    use LogsActivity;
    use SoftDeletes;

    protected static $logFillable = true;
    protected static $logName = 'enviadadospncp';

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'envia_dados_pncp';
    protected $fillable = [
        'id',
        'pncpable_type',
        'pncpable_id',
        'json_enviado_inclusao',
        'json_enviado_alteracao',
        'link_pncp',
        'situacao',
        'contrato_id',
        'sequencialPNCP',
        'tipo_contrato',
        'linkArquivoEmpenho',
        'retorno_pncp',
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */



    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function status()
    {
        return $this->belongsTo(Codigoitem::class, 'situacao');
    }

    public function minuta_empenho()
    {
        return $this->morphedByMany(MinutaEmpenho::class, 'pncpable');
    }

    public function contrato_historico()
    {
        return $this->morphedByMany(Contratohistorico::class, 'pncpable');
    }

    public function contrato_arquivos()
    {
        return $this->morphedByMany(Contratoarquivo::class, 'pncpable');
    }


}

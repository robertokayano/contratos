<?php

namespace App\Models;

use App\Http\Traits\BuscaCodigoItens;
use Backpack\CRUD\CrudTrait;
use Eduardokum\LaravelMailAutoEmbed\Models\EmbeddableEntity;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Models\CompraItemMinutaEmpenho;
use Illuminate\Database\Eloquent\Collection;

class MinutaEmpenho extends Model
{
    use CrudTrait;
    use LogsActivity;
    use SoftDeletes;
    use BuscaCodigoItens;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected static $logFillable = true;
    protected static $logName = 'minuta_empenhos';

    protected $table = 'minutaempenhos';

    protected $guarded = [
        'id'
    ];

    protected $fillable = [
        'amparo_legal_id',
        'compra_id',
        'conta_contabil_passivo_anterior',
        'data_emissao',
        'descricao',
        'etapa',
        'fornecedor_compra_id',
        'fornecedor_empenho_id',
        'informacao_complementar',
        'local_entrega',
        'numero_empenho_sequencial',
        'passivo_anterior',
        'processo',
        'saldo_contabil_id',
        'situacao_id',
        'taxa_cambio',
        'tipo_empenho_id',
        'tipo_minuta_empenho',
        'unidade_id',
        'valor_total',
        'tipo_empenhopor_id',
        'contrato_id',
        'numero_cipi',
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    // retorna minutas de empenho, que tenham força de contrato
    public function getTodosEmpenhosComForcaDeContratoByUasgByNumeroEmpenho($codigoUnidadeEmitente, $numeroEmpenho)
    {
        $minutas = MinutaEmpenho::where('minutaempenhos.mensagem_siafi', $numeroEmpenho)
            ->select(
                'minutaempenhos.updated_at as minutaempenho_data_criacao',
                'minutaempenhos.id as minutaempenho_id', 'minutaempenhos.contrato_id as contrato_id',
                'minutaempenhos.compra_id as compra_id', 'minutaempenhos.mensagem_siafi as numero_empenho',
                'minutaempenhos.unidade_id as minutaempenho_unidade_id',
                'saldo_contabil.unidade_id as saldocontabil_unidade_id',
                'unidades.codigo as unidade_codigo', 'unidades.nomeresumido as unidade_nomeresumido',
                'unidades.id as unidade_id', 'unidades.nome as unidade_nomecompleto',
                'fornecedor_compra.nome as fornecedor_compra_nome', 'fornecedor_compra.cpf_cnpj_idgener as fornecedor_compra_cpf_cnpj_idgener',
                'fornecedor_empenho.nome as fornecedor_empenho_nome', 'fornecedor_empenho.cpf_cnpj_idgener as fornecedor_empenho_cpf_cnpj_idgener',
                'empenhos.empenhado as empenho_valor_empenhado', 'empenhos.aliquidar as empenho_valor_a_liquidar', 'empenhos.liquidado as empenho_valor_liquidado',
                'empenhos.pago as empenho_valor_pago', 'empenhos.rpinscrito as empenho_valor_rpinscrito', 'empenhos.rpaliquidar as empenho_valor_rpaliquidar',
                'empenhos.rpliquidado as empenho_valor_rpliquidado', 'empenhos.rppago as empenho_valor_rppago',
                'empenhos.unidade_id as empenho_unidade_id',
                'empenhos.id as empenho_id',
                'naturezadespesa.codigo as naturezadespesa_codigo', 'naturezadespesa.descricao as naturezadespesa_descricao',
                'tipo_empenhopor.descricao as minutaempenho_tipo_empenhopor',
                'orgaos.nome as orgao_nome', 'orgaos.codigo as orgao_codigo'
                ,'contratos.numero as contrato_numero'
            )
            ->join('saldo_contabil', 'saldo_contabil.id', 'minutaempenhos.saldo_contabil_id')
            ->join('unidades', 'unidades.id', 'saldo_contabil.unidade_id')
            ->join('fornecedores as fornecedor_compra', 'fornecedor_compra.id', 'minutaempenhos.fornecedor_compra_id')
            ->join('fornecedores as fornecedor_empenho', 'fornecedor_empenho.id', 'minutaempenhos.fornecedor_empenho_id')
            ->leftjoin('empenhos', 'empenhos.numero', 'minutaempenhos.mensagem_siafi')              // tem que ser left
            ->leftjoin('naturezadespesa', 'naturezadespesa.id', 'empenhos.naturezadespesa_id')
            ->leftjoin('codigoitens as tipo_empenho', 'minutaempenhos.tipo_empenho_id', 'tipo_empenho.id')
            ->leftjoin('codigoitens as tipo_empenhopor', 'minutaempenhos.tipo_empenhopor_id', 'tipo_empenhopor.id')
            ->leftjoin('orgaos', 'orgaos.id', 'unidades.orgao_id')
            ->leftjoin('contrato_minuta_empenho_pivot', 'contrato_minuta_empenho_pivot.minuta_empenho_id', 'minutaempenhos.id')
            ->leftjoin('contratos', 'contratos.id', 'contrato_minuta_empenho_pivot.contrato_id')
            ->where('unidades.codigo', $codigoUnidadeEmitente)
            ->where('minutaempenhos.contrato_id', null)
            ->where('minutaempenhos.empenhocontrato', false)
            // ->where('empenhos.unidade_id', 'unidades.unidade_id')    // este where não pode ser utilizado, pois temos o leftjoin (precisa ser left) para empenhos.
            ->get();

        // vamos varrer o array e verificar a unidade, por conta do leftjoin que não podemos usar com o where
        $arrayFinal = array();
        foreach ($minutas as $minutaVerificarUnidade) {
            $unidadeDaMinuta = $minutaVerificarUnidade['saldocontabil_unidade_id'];
            $unidadeDoEmpenho = $minutaVerificarUnidade['empenho_unidade_id'];
            if ($unidadeDoEmpenho == $unidadeDaMinuta) {
                array_push($arrayFinal, $minutaVerificarUnidade);
            }
        }

        return $arrayFinal;
    }

    // verifica se a minuta é de contrato - retorn true se for.
    public function verificarSeMinutaEDeContrato($codigoUnidadeEmitente, $numeroEmpenho)
    {
        $minutas = MinutaEmpenho::where('minutaempenhos.mensagem_siafi', $numeroEmpenho)
            ->select(
                'saldo_contabil.unidade_id as saldocontabil_unidade_id',
                'empenhos.unidade_id as empenho_unidade_id'
            )
            ->join('contratos', 'contratos.id', 'minutaempenhos.contrato_id')   // aqui verificamos se o empenho é do tipo contrato
            ->join('saldo_contabil', 'saldo_contabil.id', 'minutaempenhos.saldo_contabil_id')
            ->join('unidades', 'unidades.id', 'saldo_contabil.unidade_id')
//            ->join('unidades', 'unidades.id', 'minutaempenhos.unidade_id')
            ->join('fornecedores as fornecedor_compra', 'fornecedor_compra.id', 'minutaempenhos.fornecedor_compra_id')
            ->join('fornecedores as fornecedor_empenho', 'fornecedor_empenho.id', 'minutaempenhos.fornecedor_empenho_id')
            ->leftjoin('empenhos', 'empenhos.numero', 'minutaempenhos.mensagem_siafi')              // tem que ser left
            ->leftjoin('naturezadespesa', 'naturezadespesa.id', 'empenhos.naturezadespesa_id')
            ->where('unidades.codigo', $codigoUnidadeEmitente)
            ->where('minutaempenhos.contrato_id', '<>', null)   // aqui verificamos se o empenho é do tipo contrato
            ->where('minutaempenhos.empenhocontrato', false)
            ->get();
        // vamos varrer o array e verificar a unidade, por conta do leftjoin que não podemos usar com o where
        $arrayFinal = array();
        foreach ($minutas as $minutaVerificarUnidade) {
            $unidadeDaMinuta = $minutaVerificarUnidade['saldocontabil_unidade_id'];
            $unidadeDoEmpenho = $minutaVerificarUnidade['empenho_unidade_id'];
            if ($unidadeDoEmpenho == $unidadeDaMinuta) {
                array_push($arrayFinal, $minutaVerificarUnidade);
            }
        }
        if (count($arrayFinal) > 0) {
            return true;
        }
        return false;
    }

    /**
     * Retorna dados da Minuta de Empenho para apresentação
     *
     * @return array
     */
    public function retornaListagem()
    {
        $ug = session('user_ug');
        $listagem = MinutaEmpenho::where('id', 1)->get();

//        $listagem->select([
//            'apropriacoes.id',
//            'competencia',
//            'nivel',
//            'valor_liquido',
//            'valor_bruto',
//            'fase_id',
//            'F.fase',
//            'arquivos'
//        ])->where('ug', $ug);

        return $listagem;
    }

    public function retornaAmparoPorMinuta()
    {
        $return = AmparoLegal::select(['amparo_legal.id', DB::raw("ato_normativo ||
                    case when (artigo is not null)  then ' - Artigo: ' || artigo else '' end ||
                    case when (paragrafo is not null)  then ' - Parágrafo: ' || paragrafo else '' end ||
                    case when (amparo_legal.inciso is not null)  then ' - Inciso: ' || amparo_legal.inciso else '' end ||
                    case when (alinea is not null)  then ' - Alinea: ' || alinea else '' end
                    as campo_api_amparo")
        ])
            ->where('minutaempenhos.id', $this->id)
            ->where('amparo_legal.codigo', '<>', '');

        if ($this->tipo_empenhopor->descricao === 'Contrato') {
            $return->join('contratos', 'contratos.modalidade_id', '=', 'amparo_legal.modalidade_id')
                ->join('minutaempenhos', 'minutaempenhos.contrato_id', '=', 'contratos.id');
            return $return->pluck('campo_api_amparo', 'amparolegal.id')->toArray();
        }

        $return->join('compras', 'compras.modalidade_id', '=', 'amparo_legal.modalidade_id')
            ->join('minutaempenhos', 'minutaempenhos.compra_id', '=', 'compras.id');


        return $return->pluck('campo_api_amparo', 'amparolegal.id')->toArray();
    }

    public function retornaAmparoPorMinutadeContrato()
    {
        return AmparoLegal::select(['amparo_legal.id', DB::raw("ato_normativo ||
                    case when (artigo is not null)  then ' - Artigo: ' || artigo else '' end ||
                    case when (paragrafo is not null)  then ' - Parágrafo: ' || paragrafo else '' end ||
                    case when (amparo_legal.inciso is not null)  then ' - Inciso: ' || amparo_legal.inciso else '' end ||
                    case when (alinea is not null)  then ' - Alinea: ' || alinea else '' end
                    as campo_api_amparo")
        ])->join('contratos', 'contratos.modalidade_id', '=', 'amparo_legal.modalidade_id')
            ->join('minutaempenhos', 'minutaempenhos.contrato_id', '=', 'contratos.id')
            ->where('minutaempenhos.id', $this->id)
            ->where('amparo_legal.codigo', '<>', '')
            ->pluck('campo_api_amparo', 'amparolegal.id')->toArray();
//        ;dd($teste->getBindings(),$teste->toSql());
    }

    /**
     * Método Necessário para mostrar valor escolhido do campo multiselect após submeter
     * quando o attribute o campo estiver referenciando um alias na consulta da API
     * obrigatório quando utilizar campo select2_from_ajax_multiple_alias
     * @return  string nome_minuta_empenho
     */
    public function retornaConsultaMultiSelect($item)
    {
        $minuta = $this
            ->select(['minutaempenhos.id',
                DB::raw("CONCAT(minutaempenhos.mensagem_siafi, ' - ', to_char(data_emissao, 'DD/MM/YYYY')  )
                                 as nome_minuta_empenho")])
            ->distinct('minutaempenhos.id')
            ->join('compras', 'minutaempenhos.compra_id', '=', 'compras.id')
            ->join('codigoitens', 'codigoitens.id', '=', 'compras.modalidade_id')
            ->join('unidades', 'minutaempenhos.unidade_id', '=', 'unidades.id')
            ->leftJoin('contrato_minuta_empenho_pivot', 'minutaempenhos.id', '=', 'contrato_minuta_empenho_pivot.minuta_empenho_id')
            ->where('minutaempenhos.id', $item->id)
            ->first();

        return $minuta->nome_minuta_empenho;
    }

    public function atualizaFornecedorCompra($fornecedor_id)
    {
        $this->fornecedor_compra_id = $fornecedor_id;
        $this->fornecedor_empenho_id = $fornecedor_id;
        $this->etapa = 3;
        $this->update();
    }

    public function getUnidade()
    {

        $unidade = $this->unidade_id()->first();
        return $unidade->codigo . ' - ' . $unidade->nomeresumido;
    }

    public function getUgEmitente()
    {
        if (isset($this->saldo_contabil)) {
            $ug = $this->saldo_contabil->unidade_id()->first();
            return $ug->codigo . ' - ' . $ug->nomeresumido;
        }
        return '';
    }

    public function getUnidadeCompra()
    {
        $unidade = $this->compra->unidade_origem()->first();
        return $unidade->codigo . ' - ' . $unidade->nomeresumido;
    }

    public function getSituacao()
    {
        return $this->situacao->descricao;
    }

    /**
     * Retorna a situação da remessa recebida
     * @param string $remessa_id
     * @return String
     */
    public function getSituacaoRemessa(string $remessa_id): string
    {
        return $this->remessa()->where('id', $remessa_id)->select('situacao_id')->first()->situacao->descricao;
    }

    public function getFornecedorEmpenho()
    {
        $fornecedor = $this->fornecedor_empenho()->first();
        if ($fornecedor) {
            return $fornecedor->cpf_cnpj_idgener . ' - ' . $fornecedor->nome;
        }
        return '';
    }

    /**
     * Retorna descrição do Tipo do Empenho
     *
     * @return string
     */
    public function getTipoEmpenho()
    {
        return $this->tipo_empenho->descricao ?? '';
    }

    public function getTipoEmpenhoPor()
    {
        return $this->tipo_empenhopor->descricao ?? '';
    }

    /**
     * Retorna descrição do Amparo Legal
     *
     * @return string
     */
    public function getAmparoLegal()
    {

        if (isset($this->amparo_legal)) {
            $artigo = isset($this->amparo_legal->artigo) ? ' - Artigo: ' . $this->amparo_legal->artigo : '';
            $paragrafo = isset($this->amparo_legal->paragrafo) ? ' - Parágrafo: ' . $this->amparo_legal->paragrafo : '';
            $inciso = isset($this->amparo_legal->inciso) ? ' - Inciso: ' . $this->amparo_legal->inciso : '';
            $alinea = isset($this->amparo_legal->alinea) ? ' - Alínea: ' . $this->amparo_legal->alinea : '';

            return $this->amparo_legal->ato_normativo . $artigo . $paragrafo . $inciso . $alinea;
        }
        return '';
    }

    public function getItens($minutaempenhos_remessa_id)
    {
        $tipo_contrato_id = $this->retornaIdCodigoItem('Tipo Empenho Por', 'Contrato');
//        dump($tipo_contrato_id);

        //SE FOR CONTRATO
        if ($this->tipo_empenhopor_id == $tipo_contrato_id) {
            return $this->contratoItemMinutaEmpenho()
                ->join(
                    'naturezasubitem',
                    'naturezasubitem.id',
                    '=',
                    'contrato_item_minuta_empenho.subelemento_id'
                )
                ->join(
                    'codigoitens',
                    'codigoitens.id',
                    '=',
                    'contrato_item_minuta_empenho.operacao_id'
                )
                ->join(
                    'contratoitens',
                    'contratoitens.id',
                    '=',
                    'contrato_item_minuta_empenho.contrato_item_id'
                )
                ->where('contrato_item_minuta_empenho.minutaempenhos_remessa_id', $minutaempenhos_remessa_id)
                ->select(
                    DB::raw('contratoitens.numero_item_compra              AS "numeroItemCompra"'),
                    DB::raw('contrato_item_minuta_empenho.numseq                 AS "numeroItemEmpenho"'),
                    DB::raw('CEIL(contrato_item_minuta_empenho.quantidade)             AS "quantidadeEmpenhada"'),
                    DB::raw('naturezasubitem.codigo AS subelemento'),
                    DB::raw('LEFT(codigoitens.descres, 1)     AS "tipoEmpenhoOperacao"'),
                    DB::raw('contratoitens.valorunitario AS "valorUnitarioItem"'),
                    DB::raw('NULL AS "tipoUASG"'),
                    DB::raw('(SELECT SUM(valor)
                        FROM contrato_item_minuta_empenho cime
                        WHERE cime.minutaempenho_id = contrato_item_minuta_empenho.minutaempenho_id
                          AND cime.minutaempenhos_remessa_id =
                              contrato_item_minuta_empenho.minutaempenhos_remessa_id) AS "valorTotalEmpenho"
                    ')
                )
                ->get();
        }

        return
            $this->compraItemMinutaEmpenho()
                ->join(
                    'naturezasubitem',
                    'naturezasubitem.id',
                    '=',
                    'compra_item_minuta_empenho.subelemento_id'
                )
                ->join(
                    'codigoitens',
                    'codigoitens.id',
                    '=',
                    'compra_item_minuta_empenho.operacao_id'
                )
                ->join(
                    'compra_items',
                    'compra_items.id',
                    '=',
                    'compra_item_minuta_empenho.compra_item_id'
                )
                ->join(
                    'compra_item_fornecedor',
                    'compra_item_fornecedor.compra_item_id',
                    '=',
                    'compra_items.id'
                )
                ->join(
                    'compra_item_unidade',
                    'compra_item_unidade.compra_item_id',
                    '=',
                    'compra_items.id'
                )
                ->where('compra_item_minuta_empenho.minutaempenhos_remessa_id', $minutaempenhos_remessa_id)
                ->select(
                    DB::raw('compra_items.numero              AS "numeroItemCompra"'),
                    DB::raw('compra_item_minuta_empenho.numseq                 AS "numeroItemEmpenho"'),
                    DB::raw('CEIL(compra_item_minuta_empenho.quantidade)             AS "quantidadeEmpenhada"'),
                    DB::raw('naturezasubitem.codigo AS subelemento'),
                    DB::raw('LEFT(codigoitens.descres, 1)     AS "tipoEmpenhoOperacao"'),
                    DB::raw('compra_item_fornecedor.valor_unitario AS "valorUnitarioItem"'),
                    DB::raw('compra_item_unidade.tipo_uasg AS "tipoUASG"'),
                    DB::raw('(SELECT SUM(valor)
                        FROM compra_item_minuta_empenho cime
                        WHERE cime.minutaempenho_id = compra_item_minuta_empenho.minutaempenho_id
                          AND cime.minutaempenhos_remessa_id =
                              compra_item_minuta_empenho.minutaempenhos_remessa_id) AS "valorTotalEmpenho"
                    ')
                )
                ->get();
    }

    public function serializaPNCP($cnpjCompra, $sequencialCompra)
    {
        return [
            'cnpjCompra' => $cnpjCompra,
            'anoCompra' => explode('/', $this->compra->numero_ano)[1],
            'sequencialCompra' => intval($sequencialCompra),
            'tipoContratoId' => config('api-pncp.tipo_contrato')["99"],//Constante
            'numeroContratoEmpenho' => $this->mensagem_siafi,
            'anoContrato' => explode('NE', $this->mensagem_siafi)[0],
            'processo' => $this->processo,
            'categoriaProcessoId' => config('api-pncp.categoria_contrato')["Serviços"],//["Empenho Força de Contrato"],//Constante
            'niFornecedor' => $this->trataFornecedor(),
            'tipoPessoaFornecedor' => config('api-pncp.tipo_fornecedor')[$this->fornecedor_empenho->tipo_fornecedor],
            'nomeRazaoSocialFornecedor' => $this->fornecedor_empenho->nome,
            'receita' => false,
            'codigoUnidade' => $this->saldo_contabil->unidade_id()->first()->codigo,
            'objetoContrato' => $this->descricao,
            'valorInicial' => $this->valor_total,
            'numeroParcelas' => 1,
            'valorParcela' => $this->valor_total,
            'valorGlobal' => $this->valor_total,
            'dataAssinatura' => $this->data_emissao,
            'dataVigenciaInicio' => $this->data_emissao,
            'dataVigenciaFim' => explode('NE', $this->mensagem_siafi)[0] . '-12-31',
            'valorAcumulado' => $this->valor_total,
            'cnpjOrgaoSubRogado' => null,
            'codigoUnidadeSubRogada' => null,
            'niFornecedorSubContratado' => null,
            'tipoPessoaFornecedorSubContratado' => null,
            'nomeRazaoSocialFornecedorSubContratado' => null,
            'informacaoComplementar' => $this->informacao_complementar
        ];
    }

    public function trataFornecedor()
    {
        $retorno = null;
        switch ($this->fornecedor_empenho->tipo_fornecedor) {
            case 'UG':
                $ug = Unidade::where($this->fornecedor_empenho->cpf_cnpj_idgener, 'codigo')->first();
                $retorno = @$ug->cnpj;
                break;
            case 'IDGENERICO':
                $retorno = $this->fornecedor_empenho->cpf_cnpj_idgener;
                break;
            default:
                $retorno = preg_replace("/[^0-9]/", "", $this->fornecedor_empenho->cpf_cnpj_idgener);
        }
        return $retorno;
    }

    public function possuiAmparo($amparo)
    {

        if (isset($this->amparo_legal)) {
            if ($this->amparo_legal->ato_normativo == $amparo) {
                return true;
            }
        }
        return false;
    }

    public function serializaArquivoPNCP()
    {
        $dados_empenho = [
            'ugemitente' => $this->saldo_contabil->unidade_id()->first()->codigo,
            'anoempenho' => substr($this->mensagem_siafi,0,4),
            'numempenho' => (int)substr($this->mensagem_siafi,6,6),
        ];

        $nome_arquivo = $dados_empenho['ugemitente'] . '_' . $dados_empenho['anoempenho'] . 'NE' .  str_pad($dados_empenho['numempenho'], 6, "0", STR_PAD_LEFT);
        $path = 'empenhos_pdf/'.$dados_empenho['ugemitente'] . '_' . $dados_empenho['anoempenho'] . 'NE' .  str_pad($dados_empenho['numempenho'], 6, "0", STR_PAD_LEFT).'/';

        //$filepath = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
        $filepath = config('app.app_path')."storage/app";
        return $filepath . "/" . $path . $nome_arquivo.'.pdf';

    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function envia_dados_pncp()
    {
        return $this->morphToMany(EnviaDadosPncp::class, 'pncpable');
    }

    public function amparo_legal()
    {
        return $this->belongsTo(AmparoLegal::class, 'amparo_legal_id');
    }

    public function compra()
    {
        return $this->belongsTo(Compra::class, 'compra_id');
    }

    public function empenho_dados()
    {
        return $this->hasMany(SfOrcEmpenhoDados::class, 'minutaempenho_id');
    }

    public function fornecedor_compra()
    {
        return $this->belongsTo(Fornecedor::class, 'fornecedor_compra_id');
    }

    public function fornecedor_empenho()
    {
        return $this->belongsTo(Fornecedor::class, 'fornecedor_empenho_id');
    }

    public function saldo_contabil()
    {
        return $this->belongsTo(SaldoContabil::class, 'saldo_contabil_id');
    }

    public function tipo_empenho()
    {
        return $this->belongsTo(Codigoitem::class, 'tipo_empenho_id');
    }

    public function tipo_empenhopor()
    {
        return $this->belongsTo(Codigoitem::class, 'tipo_empenhopor_id');
    }

    public function unidade_id()
    {
        return $this->belongsTo(Unidade::class, 'unidade_id');
    }

    public function passivo_anterior()
    {
        return $this->hasMany(ContaCorrentePassivoAnterior::class, 'minutaempenho_id');
    }

    public function remessa()
    {
        return $this->hasMany(MinutaEmpenhoRemessa::class, 'minutaempenho_id');
    }

    public function situacao()
    {
        return $this->belongsTo(Codigoitem::class, 'situacao_id');
    }

    public function contrato()
    {
        return $this->belongsToMany(
            'App\Models\Contrato',
            'contrato_minuta_empenho_pivot',
            'minuta_empenho_id',
            'contrato_id'
        );
    }

    public function compraItemMinutaEmpenho()
    {
        return $this->hasMany(CompraItemMinutaEmpenho::class, 'minutaempenho_id');
    }

    public function contratoItemMinutaEmpenho()
    {
        return $this->hasMany(ContratoItemMinutaEmpenho::class, 'minutaempenho_id');
    }

    public function contrato_vinculado()
    {
        return $this->belongsTo(Contrato::class, 'contrato_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    public function getCompraModalidadeAttribute()
    {
        $compra = $this->compra()->first()->modalidade()->first();
        return $compra->descres . ' - ' . $compra->descricao;
    }

    public function getTipoCompraAttribute()
    {
        return $this->compra()->first()->tipo_compra()->first()->descricao;
    }

    public function getNumeroAnoAttribute()
    {
        return $this->compra()->first()->numero_ano;
    }

    public function getIncisoAttribute()
    {
        return $this->compra()->first()->inciso;
    }

    public function getLeiAttribute()
    {
        return $this->compra()->first()->lei;
    }

    public function getMaxRemessaAttribute()
    {
        return $this->remessa()->max('id');
    }

    public function getSituacaoDescricaoAttribute()
    {
        return $this->situacao()->first()->descricao;
    }

    public function getEmpenhoPorAttribute()
    {
        return $this->tipo_empenhopor->descricao;
    }

    public function getFornecedorEmpenhoCpfcnpjidgenerSessaoAttribute()
    {
        $fornecedor = $this->fornecedor_empenho()->first();
        return $fornecedor->cpf_cnpj_idgener ?? '';
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}

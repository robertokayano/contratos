<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Models\Movimentacaocontratoconta;
use App\Models\Contratoconta;
class Lancamento extends Model
{
    use CrudTrait;
    use LogsActivity;
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'lancamentos';
    protected $fillable = [
        'contratoterceirizado_id', 'encargo_id', 'valor', 'movimentacao_id', 'salario_atual', 'encargo_nome', 'encargo_percentual'
    ];
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getValorTotalLancamentosByIdMovimentacao($idMovimentacao){
        $valorTotal = Lancamento::where('movimentacao_id', '=', $idMovimentacao)->sum('valor');
        // \Log::info('total mov = '.$valorTotal);
        return $valorTotal;
    }
    public function getSalarioContratoTerceirizado(){
        $objContratoTerceirizado = Contratoterceirizado::find($this->contratoterceirizado_id);
        return $objContratoTerceirizado->salario;
    }
    public function getNomePessoaContratoTerceirizado(){
        $objContratoTerceirizado = Contratoterceirizado::find($this->contratoterceirizado_id);
        return $objContratoTerceirizado->nome;
    }
    public function getTipoEncargo(){
        $objEncargo = Encargo::find($this->encargo_id);
        $objCodigoItem = Codigoitem::find($objEncargo->tipo_id);
        return $descricao= $objCodigoItem->descricao;
    }
    // Com a mudança na regra do grupo A, que passou para a tabela da conta, o encargo_id pode chegar aqui em branco.
    // este método se repete em Extratocontratoconta.php
    /**
     * Este método serve apenas para Conta Vinculada pelo Caderno e não serve para Conta Vinculada pela resolução 169
     * A partir de 10/2021, existe a possibilidade do lançamento ter sido para Realocado. Vamos verificar se é o caso.
     */
    public function getTipoEncargoOuGrupoA(){
        if($this->encargo_id != null){
            $idEncargo = $this->encargo_id;
            $objEncargo = Encargo::find($idEncargo);
            $objCodigoItem = Codigoitem::find($objEncargo->tipo_id);
            return $descricao= $objCodigoItem->descricao;
        }
        if(trim($this->encargo_nome) == 'Realocado'){return $this->encargo_nome;}
        // return 'Incidência do Submódulo 2.2 sobre férias, 1/3 (um terço) constitucional de férias e 13o (décimo terceiro) salário';
        // return 'Incidência do Grupo A sobre férias, abono e 13o. salário';          // alterado em 08/10/2021 a pedido da Anne
        return 'Incidência do Submódulo 2.2 sobre férias, 1/3 (um terço) constitucional de férias e 13o (décimo terceiro) salário';    // alterado em 08/10/2021 a pedido da Anne
    }
    public function getPercentualEncargo(){
        $objEncargo = Encargo::find($this->encargo_id);
        return $objEncargo->percentual;
    }
    // se não chegar com id do encargo, se trata de grupo a ou submódulo 2.2
    public function getPercentualEncargoOuGrupoA(){
        // a partir de 10/2021, existe a situação Realocado, para a qual, o percentual é zero. Vamos verificar se é o caso.
        if(trim($this->encargo_nome) == 'Realocado'){return '0.00';}
        if( $this->encargo_id != null ){
            // return 'ok';
            // aqui é para os encargos - irão chegar aqui com id
            $objEncargo = Encargo::find($this->encargo_id);
            return $objEncargo->percentual;
        } else {
            // aqui é para grupo A ou submodulo 2.2
            $idLancamento = $this->id;
            $idContratoConta = Contratoconta::getIdContratocontaByidLancamento($idLancamento);
            $objContratoconta = Contratoconta::where('id', $idContratoConta)->first();
            /**
             * 27/04/2021
             * Após reunião com o Gabriel, o percentual do lançamento para este caso, irá variar de acordo com o tipo da movimentação.
             * Se for provisão, será o percentual grupo a 13 e férias
             * Se for libaeração, será o percentual do submódulo 2.2
             *
             * Ambos estão na tabela contratocontas.
             *
             */
            $idMovimentacao = $this->movimentacao_id;
            $tipoMovimentacao = $this->getTipoMovimentacao();
            if($tipoMovimentacao == 'Liberação'){
                return $percentual = $objContratoconta->percentual_submodulo22;
            } elseif($tipoMovimentacao == 'Provisão'){
                return $percentual = $objContratoconta->percentual_grupo_a_13_ferias;
            } elseif($tipoMovimentacao == 'Repactuação'){
                return $percentual = $objContratoconta->percentual_grupo_a_13_ferias;
            }
        }
    }
    public function formatValor(){
        return number_format($this->valor, 2, ',', '.');
    }
    public function getTipoMovimentacao(){
        $idMovimentacao = $this->movimentacao_id;
        $objMovimentacao = Movimentacaocontratoconta::find($idMovimentacao);
        return $objMovimentacao->getTipoMovimentacao();
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}

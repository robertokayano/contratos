<?php

namespace App\Http\Traits;

use Illuminate\Support\Facades\Storage;

trait ArquivoBase64ToPdf
{
    public function salvarArquivoBase64ToPdf(string $path, string $nomearquivo, string $file)
    {
        $return = false;

        if($file) {
            $name = $nomearquivo.'.pdf';
            Storage::put($path.$name, base64_decode($file));
            $return = true;
        }

        return $return;
    }

}

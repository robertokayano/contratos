<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Codigoitem;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ContratoarquivoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $arquivos = ($this->arquivos) ? 'mimetypes:application/pdf' : "NULL";

        $rules = [
            'contrato_id' => 'required',
            'tipo' => 'required',
            'contratohistorico_id' => Rule::requiredIf(function () {
                $tipo = Codigoitem::find($this->tipo);
                if (isset($tipo->descricao)) {
                    if (in_array($tipo->descricao, config('app.tipos_obrigatorios_arquivo'))) {
                        return true;
                    }
                }
                return false;
            }),
            'processo' => 'required|max:255',
            'descricao' => 'required|max:255',
            'arquivos.*' => $arquivos,
        ];


        return $rules;
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'contratohistorico_id.required' => 'O campo \'Termo\' é obrigatório, quando o \'Tipo\' é \'Termo Aditivo\', \'Termo Apostilamento\', ou \'Termo Rescisão\' ',
        ];
    }
}

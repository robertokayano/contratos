<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class ContratofaturaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        /*'data_assinatura' => "required|date|after:{$this->data_limiteinicio}|before_or_equal:{$this->data_atual}",
        'vigencia_inicio' => 'required|date|after_or_equal:data_assinatura|before:vigencia_fim',
        'vigencia_fim' => "required|date|after:vigencia_inicio|before:{$this->data_limitefim}",*/

        $this->hoje = date('Y-m-d');
        //$this->hoje = date('d-m-Y');

        return [
            'contrato_id' => 'required',
            'tipolistafatura_id' => 'required',
            'numero' => 'required|max:17',
            'emissao' => "required|date|before_or_equal:{$this->hoje}",
            'vencimento' => 'required|date|after_or_equal:emissao',
            'valor' => 'required',
            'processo' => 'required|max:20',
            'protocolo' => "required|date|after_or_equal:emissao|before_or_equal:ateste",
            'ateste' => "required|date|after_or_equal:protocolo",
            'repactuacao' => 'required',
            'mesref' => 'required',
            'anoref' => 'required',
            'infcomplementar' => 'max:255',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'emissao' => "Dt. Emissão",
            'vencimento' => "Dt. Vencimento",
            'protocolo' => "Dt. Protocolo",
            'ateste' => "Dt. Ateste"
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'emissao.before_or_equal' => "O campo :attribute deve ser uma data anterior ou igual a Hoje!",
            'vencimento.after_or_equal' => "O campo :attribute deve ser uma data posterior ou igual a Dt. Emissão!",
            'protocolo.after_or_equal' => "O campo :attribute deve ser uma data posterior ou igual a Dt. Emissão!",
            'ateste.before_or_equal' => "O campo :attribute deve ser uma data posterior ou igual a Dt. Protocolo!"
        ];
    }
}

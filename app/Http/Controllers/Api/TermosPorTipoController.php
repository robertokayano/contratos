<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Codigoitem;
use App\Models\Contratohistorico;
use Illuminate\Support\Facades\Log;

class TermosPorTipoController extends Controller
{
    public function index(Request $request)
    {
        $search_term = $request->input('q');
        $form = collect($request->input('form'))->pluck('value', 'name');

        $options = Contratohistorico::query();

        // if no category has been selected, show no options
        if (!$form['tipo']) {
            return [];
        }

        // if a category has been selected, only show articles in that category
        if ($form['tipo']) {
            $tipo = Codigoitem::find($form['tipo'])->descricao;
            if(in_array($tipo, config('app.tipos_obrigatorios_arquivo'))){

                ($tipo == 'Termo Apostilamento') ? $tipo = 'Termo de Apostilamento': '';
                ($tipo == 'Termo Rescisão') ? $tipo = 'Termo de Rescisão': '';

                $options = $options->whereIn('tipo_id',function($q) use ($tipo){
                    $q->select('id')->from('codigoitens')->where('descricao',$tipo);
                })->where('contrato_id', $form['contrato_id']);
                
            }else{
                return [];
            }
        }

        if ($search_term) {
            return $options->where('numero', 'ilike', '%' . strtoupper($search_term) . '%')
            ->orderBy('numero')
            ->paginate(10);
        }

        return $options->paginate(10);
    }

    public function show($id)
    {
        return Contratohistorico::find($id);
    }
}

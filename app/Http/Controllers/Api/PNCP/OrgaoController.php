<?php

namespace App\Http\Controllers\Api\PNCP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Orgao;
use Exception;
use Illuminate\Support\Facades\DB;

class OrgaoController extends PncpController
{
    public function __construct(){
        
        parent::__construct();
    }

    public function insereOrgao($id){
        try{
        
        $orgao = Orgao::where('id',$id)->first();

        $url = $this->montaUrl("v1/orgaos",null);            
        $body = $this->montaBody(
            $orgao->serializaPNCP()
        );
        
        $response = $this->post($url, $this->montaHeader(true, $this->headerPadrao), $body);
        
        return $response;

        }catch(Exception $e){
            throw $e;
        }
    }

    public function consultaOrgao(String $cnpj){

        $params = ['cnpj'=>$cnpj];
        $url = $this->montaUrl("v1/orgaos/".$cnpj."/unidades",$params);
        
        $response = $this->get($url, $this->montaHeader(true, []));
        
        return json_decode($response->getBody()->getContents(), true);
    }
}

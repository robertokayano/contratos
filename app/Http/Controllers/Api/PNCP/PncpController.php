<?php

namespace App\Http\Controllers\Api\PNCP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\RestAPIController;
use App\Models\PncpUsuario;
use App\PncpToken;
use Exception;
use Illuminate\Cookie\Middleware\EncryptCookies;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Crypt;

class PncpController extends RestAPIController
{
    protected $token;
    protected $baseUrl;
    public function __construct(){
        $this->token = PncpUsuario::where('ativo', true)->pluck('token')->first();
        //$this->baseUrl = "https://hom.pncp.gov.br/api/pncp/";//config('api-pncp.url');
        $this->baseUrl = config('api-pncp.url');
        parent::__construct();
    }
    function buscaToken(){
        return $this->token;
    }

    function buscaBaseUrl(){
        return $this->baseUrl;
    }

}

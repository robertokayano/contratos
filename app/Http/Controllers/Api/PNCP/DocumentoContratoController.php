<?php

namespace App\Http\Controllers\Api\PNCP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contratoarquivo;
use App\Models\MinutaEmpenho;
use Exception;

class DocumentoContratoController extends PncpController
{
    public function __construct(){
        
        parent::__construct();
    }

    public function consultarDocumentoContrato(String $cnpj,String $ano,String $sequencial){

        $url = $this->montaUrl($this->urlEndpoint($cnpj, $ano, $sequencial),null);
        $response = $this->get($url, $this->montaHeader(true, []));
        
        return json_decode($response->getBody()->getContents(), true);
    }

    public function inserirDocumentoContrato($arquivo, String $cnpj, String $ano,String $sequencial){
        try{
            if($arquivo instanceof MinutaEmpenho){
                $arquivo->descricao = 'Empenho ' . $arquivo->informacao_complementar;
                $codigoItem = new \stdClass;
                $codigoItem->descres = "CONTRATO";
                $arquivo->codigoItem = $codigoItem;
            }
            $url = $this->montaUrl($this->urlEndpoint($cnpj, $ano, $sequencial),null);
            $conteudo =  $arquivo->serializaArquivoPNCP();
            $descricao = $arquivo->descricao;
            !isset($arquivo->descricao)? $descricao = $arquivo->codigoItem->descricao : '';
            $header = ["Titulo-Documento" => $descricao, "Tipo-Documento-Id"=>config('api-pncp.tipo_documento_contrato')[$arquivo->codigoItem->descres]];
            $response = $this->postMultipart($url, $this->montaHeader(true, $header), 'arquivo', $conteudo);
            //$response = $this->requestTeste(201, ['location'=>'https://hom.pncp.gov.br/api/pncp/v1/orgaos/00394460000141/contratos/2021/1'], 'request falso');
            return $response;
            
        }catch(Exception $e){
            throw $e;
        }
    }

    public function baixarDocumentoContrato(String $cnpj,String $ano,String $sequencial, String $sequencialDocumento){

        $url = $this->montaUrl($this->urlEndpoint($cnpj, $ano, $sequencial)."/".$sequencialDocumento,null);
        
        $response = $this->get($url, $this->montaHeader(true, []));
        
        return json_decode($response->getBody()->getContents(), true);
    }



    public function excluirDocumentoContrato($envio,String $cnpj,String $ano,String $sequencial, String $sequencialDocumento){
        try{
            $url = $this->montaUrl($this->urlEndpoint($cnpj, $ano, $sequencial)."/".$sequencialDocumento,null);
            $body = $this->montaBody(
                $envio->serializaExclusaoPNCP()
            );
            $response = $this->delete($url, $this->montaHeader(true, $this->headerPadrao),$body);
            return $response;
        }catch(Exception $e){
            throw $e;
        }
    }

    private function urlEndpoint(String $cnpj,String $ano,String $sequencial){
        return "v1/orgaos/".$cnpj."/contratos/".$ano."/".$sequencial."/arquivos";
    }
}

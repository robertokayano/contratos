<?php

namespace App\Http\Controllers\Api\PNCP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\RestAPIController;
use App\Models\PncpUsuario;
use App\PncpToken;
use Exception;
use Illuminate\Cookie\Middleware\EncryptCookies;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UsuarioController extends PncpController
{
    
    public function __construct(){
        
        parent::__construct();
    }

    public function getUsuario(String $identificador){

        $params = ['identificador'=>$identificador];
        $url = $this->montaUrl("v1/usuarios",$params);
        
        $response = $this->get($url, $this->montaHeader(true, []));
        
        return json_decode($response->getBody()->getContents(), true);
    }

    public function putUsuario(int $id){
        try{
            $pncpUsuario = PncpUsuario::where('id', $id)->first();

            $url = $this->montaUrl("v1/usuarios/".$pncpUsuario->id_pncp,null);
            $body = $this->montaBody(
                $pncpUsuario->serializaPNCP()
            );
            $header = array_merge($this->headerPadrao, ["Accept-Encoding" => 'gzip,deflate']);
            $response = $this->put($url, $this->montaHeader(true, $header), $body);

        }catch(Exception $e){
            throw $e;
        }
    
    }

    public function login(){
        try{
        Log::info("Logando no PNCP");
        $pncpUsuario = PncpUsuario::where('ativo', true)->first();
        $url = $this->montaUrl("v1/usuarios/login",null);            
        $body = $this->montaBody([
            'login' => $pncpUsuario->login,
            'senha' => $pncpUsuario->senha,
        ]);
        
        $response = $this->post($url, $this->montaHeader(false, $this->headerPadrao), $body);
        $pncpUsuario->token = $response->getHeader('authorization')[0];
        $pncpUsuario->save();
        return $response->getStatusCode();
        }catch(Exception $e){
            throw $e;
        }
    }

}

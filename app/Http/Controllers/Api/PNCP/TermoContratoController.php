<?php

namespace App\Http\Controllers\Api\PNCP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contrato;
use Exception;

class TermoContratoController extends PncpController
{
    public function consultarTermoContrato(String $cnpj,String $ano,String $sequencial, String $sequencialTermo){

        $url = $this->montaUrl($this->urlEndpoint($cnpj, $ano, $sequencial)."/".$sequencialTermo,null);
        
        $response = $this->get($url, $this->montaHeader(true, []));
        
        return json_decode($response->getBody()->getContents(), true);
    }

    public function retificarTermoContrato($envio,String $cnpj,String $ano,String $sequencial, String $sequencialTermo){
        try{

            $url = $this->montaUrl($this->urlEndpoint($cnpj, $ano, $sequencial)."/".$sequencialTermo,null);
            $body = $this->montaBody(
                $envio->serializaTermoPNCP(true)
            );
            
            $header = array_merge($this->headerPadrao, ["Accept-Encoding" => 'gzip,deflate']);
            
            $response = $this->put($url, $this->montaHeader(true, $header), $body);
            //$response = $this->requestTeste(201, ['location'=>'https://hom.pncp.gov.br/api/pncp/v1/orgaos/00394460000141/contratos/2021/1/termos/1'], 'request falso');
            $response->JSONEnviado = $body;
            return $response;

        }catch(Exception $e){
            throw $e;
        }
    
    }

    public function excluirTermoContrato(String $cnpj,String $ano,String $sequencial, String $sequencialTermo){
        try{
            
            $url = $this->montaUrl($this->urlEndpoint($cnpj, $ano, $sequencial)."/".$sequencialTermo,null);
            $body = $this->montaBody(
                //$envio->serializaExclusaoPNCP()
                ['justificativa' => '']
            );
            $response = $this->delete($url, $this->montaHeader(true, []), $body);
            //$response = $this->requestTeste(201, ['location'=>'https://hom.pncp.gov.br/api/pncp/v1/orgaos/00394460000141/contratos/2021/1/termos/1'], 'request falso');

            return $response;
        }catch(Exception $e){
            throw $e;
        }
    }

    public function consultarTermosDeContrato(String $cnpj,String $ano,String $sequencial){

        $url = $this->montaUrl($this->urlEndpoint($cnpj, $ano, $sequencial),null);
        
        $response = $this->get($url, $this->montaHeader(true, []));
        
        return json_decode($response->getBody()->getContents(), true);
    }

    public function inserirTermoContrato($envio, String $cnpj, String $ano,String $sequencial){
        try{

            $url = $this->montaUrl($this->urlEndpoint($cnpj, $ano, $sequencial),null);
            $body = $this->montaBody(
                $envio->serializaTermoPNCP(false)
            );
            $response = $this->post($url, $this->montaHeader(true, $this->headerPadrao), $body);
            //$response = $this->requestTeste(201, ['location'=>'https://hom.pncp.gov.br/api/pncp/v1/orgaos/00394460000141/contratos/2021/1/termos/1'], 'request falso');
            $response->JSONEnviado = $body;
            return $response;
            
        }catch(Exception $e){
            throw $e;
        }
    }

    private function urlEndpoint(String $cnpj,String $ano,String $sequencial){
        return "v1/orgaos/".$cnpj."/contratos/".$ano."/".$sequencial."/termos";
    }
}

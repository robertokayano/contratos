<?php

namespace App\Http\Controllers\Api\PNCP;

use App\Models\Contrato;
use Exception;
use App\Http\Controllers\Empenho\CompraSiasgCrudController;
use App\Models\MinutaEmpenho;
use Illuminate\Support\Facades\Log;


class ContratoControllerPNCP extends PncpController
{
    
    public function __construct(){
        
        parent::__construct();
    }

    public function consultarContrato(String $cnpj,String $ano,String $sequencial){

        $url = $this->montaUrl($this->urlEndpoint($cnpj, $ano, $sequencial),null);
        $response = $this->get($url, $this->montaHeader(true, []));
        
        return json_decode($response->getBody()->getContents(), true);
    }

    public function consultarContratoUrlMontada(String $url){
        try{
            $response = $this->get($url, $this->montaHeader(true, []));
            return json_decode($response->getBody()->getContents(), true);
        }catch(Exception $e){
            throw $e;
        }
    }

    //public function retificarContrato(int $id,String $cnpj,String $ano,String $sequencial){
    public function retificarContrato($envio, $cnpj, $idUnico, $ano, $cnpjOrgao, $sequencialPNCP){
        try{

            $url = $this->montaUrl($this->urlEndpoint($cnpjOrgao, $ano, $sequencialPNCP),null);
            $body = $this->montaBody(
                $envio->serializaPNCP($cnpj, $idUnico, true)
            );
            $header = array_merge($this->headerPadrao, ["Accept-Encoding" => 'gzip,deflate']);
            //Log::error($url);
            //Log::error($body);
            $response = $this->put($url, $this->montaHeader(true, $header), $body);
            $response->JSONEnviado = $body;
            return $response;

        }catch(Exception $e){
            throw $e;
        }
    
    }
    //remover comentários quando o Comprasnet habilitar o softdeletes para os Contratos.
    //public function excluirContrato($envio, String $cnpj,String $ano,String $sequencial){
    public function excluirContrato(String $cnpj,String $ano,String $sequencial){
        try{
            $url = $this->montaUrl($this->urlEndpoint($cnpj, $ano, $sequencial),null);
            $body = $this->montaBody(
                //$envio->serializaExclusaoPNCP()
                ['justificativa' => '']
            );
            $response = $this->delete($url, $this->montaHeader(true, []),$body);
            return $response;
        }catch(Exception $e){
            throw $e;
        }
    }

    public function inserirContrato($envio, $cnpj, $idUnico, $cnpjOrgao){
        try{

            $url = $this->montaUrl("v1/orgaos/".$cnpjOrgao."/contratos",null);
            $body = $this->montaBody(
                $envio->serializaPNCP($cnpj, $idUnico, false)
            );
            //Log::error($url);
            //Log::error($body);
            $response = $this->post($url, $this->montaHeader(true, $this->headerPadrao), $body);
            //$response = $this->requestTeste(201, ['location'=>'https://hom.pncp.gov.br/api/pncp/v1/orgaos/00394460000141/contratos/2021/1'], 'request falso');
            $response->JSONEnviado = $body;
            return $response;

        }catch(Exception $e){
            throw $e;
        }
    }

    private function urlEndpoint(String $cnpj,String $ano,String $sequencial){
        return "v1/orgaos/".$cnpj."/contratos/".$ano."/".$sequencial;
    }

}
<?php

namespace App\Http\Controllers\Api\PNCP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contrato;
use App\Models\Contratoarquivo;
use App\Models\EnviaDadosPncp;
use Exception;

class DocumentoTermoContratoController extends PncpController
{

    public function __construct(){
        
        parent::__construct();
    }

    public function consultarDocumentoTermoContrato(String $cnpj,String $ano,String $sequencial, String $sequencialTermo){

        $url = $this->montaUrl($this->urlEndpoint($cnpj, $ano, $sequencial, $sequencialTermo),null);
        $response = $this->get($url, $this->montaHeader(true, []));
        
        return json_decode($response->getBody()->getContents(), true);
    }

    public function inserirDocumentoTermoContrato($arquivo, String $cnpj, String $ano,String $sequencial,String $sequencialTermo){
        try{
            
            $url = $this->montaUrl($this->urlEndpoint($cnpj, $ano, $sequencial, $sequencialTermo),null);
            $conteudo =  $arquivo->serializaArquivoPNCP();
            $descricao = $arquivo->descricao;
            !isset($arquivo->descricao)? $descricao = $arquivo->codigoItem->descricao : '';
            $header = ["Titulo-Documento" => $descricao, "Tipo-Documento-Id"=>config('api-pncp.tipo_documento_contrato')[$arquivo->codigoItem->descres]];
            $response = $this->postMultipart($url, $this->montaHeader(true, $header), 'arquivo', $conteudo);
            //$response = $this->requestTeste(201, ['location'=>'https://hom.pncp.gov.br/api/pncp/v1/orgaos/00394460000141/contratos/2021/1'], 'request falso');

            return $response;
            
        }catch(Exception $e){
            throw $e;
        }
        
    }

    public function baixarDocumentoTermosDeContrato(String $cnpj,String $ano,String $sequencial, String $sequencialTermo, String $sequencialDocumento){

        $url = $this->montaUrl($this->urlEndpoint($cnpj, $ano, $sequencial, $sequencialTermo)."/".$sequencialDocumento,null);
        
        $response = $this->get($url, $this->montaHeader(true, []));
        
        return json_decode($response->getBody()->getContents(), true);
    }



    public function excluirDocumentoTermoContrato($envio,String $cnpj,String $ano,String $sequencial, String $sequencialTermo, String $sequencialDocumento){
        try{
            $url = $this->montaUrl($this->urlEndpoint($cnpj, $ano, $sequencial, $sequencialTermo)."/".$sequencialDocumento,null);
            $body = $this->montaBody(
                $envio->serializaExclusaoPNCP()
            );
            $response = $this->delete($url, $this->montaHeader(true, $this->headerPadrao),$body);
            return $response;
        }catch(Exception $e){
            throw $e;
        }
    }

    private function urlEndpoint(String $cnpj,String $ano,String $sequencial, String $sequencialTermo){
        return "v1/orgaos/".$cnpj."/contratos/".$ano."/".$sequencial."/termos/".$sequencialTermo."/arquivos";
    }
}
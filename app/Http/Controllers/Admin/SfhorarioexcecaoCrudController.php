<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\SfhorarioexcecaoRequest as StoreRequest;
use App\Http\Requests\SfhorarioexcecaoRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class SfhorarioexcecaoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class SfhorarioexcecaoCrudController extends CrudController
{
    public function setup()
    {
        if(!backpack_user()->can('horariosiafi_acesso')){
            abort('403', config('app.erro_permissao'));
        }
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Sfhorarioexcecao');
        $this->crud->setRoute(config('backpack.base.route_prefix') . 'admin/sfhorarioexcecao');
        $this->crud->setEntityNameStrings('Horário Siafi - Exceção', 'Horários Siafi - Exceções');


        //        $this->crud->enableExportButtons();
        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');
        $this->crud->allowAccess('show');

        (backpack_user()->can('horariosiafi_inserir')) ? $this->crud->allowAccess('create') : null;
        (backpack_user()->can('horariosiafi_editar')) ? $this->crud->allowAccess('update') : null;
        (backpack_user()->can('horariosiafi_deletar')) ? $this->crud->allowAccess('delete') : null;

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
       // $this->crud->setFromDb();
        $colunas = $this->Colunas();
        $this->crud->addColumns($colunas);

        $campos = $this->Campos();
        $this->crud->addFields($campos);

        // add asterisk for fields that are required in SfhorarioexcecaoRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function Colunas() {
        $colunas = [
            [
                'name' => 'data',
                'label' => 'Data', // Table column heading
                'type' => 'date',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'hora_inicio',
                'label' => 'Hora inicial', // Table column heading
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ],
            [
                'name' => 'hora_fim',
                'label' => 'Hora final', // Table column heading
                'type' => 'text',
                'orderable' => true,
                'visibleInTable' => true, // no point, since it's a large text
                'visibleInModal' => true, // would make the modal too big
                'visibleInExport' => true, // not important enough
                'visibleInShow' => true, // sure, why not
            ]
        ];
        return $colunas;
    }

    public function Campos()
    {
        $campos = [
            [
                'name' => 'data',
                'label' => "Data",
                'type' => 'date',
                'format' => 'd/m/Y',
            ],
            [
                'name' => 'hora_inicio',
                'label' => "Hora Inicial",
                'type' => 'time',
                'orderable' => true,
            ],
            [
                'name' => 'hora_fim',
                'label' => "Hora final",
                'type' => 'time',
                'orderable' => true,
            ],
        ];
        return $campos;
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}

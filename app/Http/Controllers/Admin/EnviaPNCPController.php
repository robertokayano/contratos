<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Api\PNCP\ContratoControllerPNCP;
use App\Http\Controllers\Api\PNCP\DocumentoContratoController;
use App\Http\Controllers\Api\PNCP\DocumentoTermoContratoController;
use App\Http\Controllers\Api\PNCP\TermoContratoController;
use App\Http\Controllers\Api\PNCP\UsuarioController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Empenho\CompraSiasgCrudController;
use App\Http\Controllers\Empenho\MinutaEmpenhoCrudController;
use App\Models\Codigoitem;
use App\Models\Contrato;
use App\Models\Contratoarquivo;
use App\Models\Contratohistorico;
use App\Models\EnviaDadosPncp;
use App\Models\MinutaEmpenho;
use Exception;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class EnviaPNCPController extends Controller
{

    protected $cc;
    protected $cscc;
    protected $dcc;
    protected $dtcc;
    protected $tcc;

    public function enviaPNCP(){
        Log::info("Envia PNCP");
        $this->insereEmpenhos();
        $this->insereContratos();
        $this->atualizaContratos();
        $this->excluirContratos();

        //pode ganhar uma rotina propria
        $this->enviaArquivosPNCP();
        return redirect('admin/pncp');
        
    }
    
    public function enviaArquivosPNCP(){
        $this->enviaArquivos();
    }

    public function enviaUnidade($key){
        $envia_pncp = EnviaDadosPncp::where('id',$key)->whereHas('status', function ($ci) {
            $ci->where('descres','INCPEN')->orWhere('descres','RETPEN')->orWhere('descres','ARQPEN')->orWhere('descres','EXCPEN');
        })->first();

        if(isset($envia_pncp)){
            $status = $envia_pncp->status->descres;
            try{
                if($envia_pncp->pncpable_type == Contratohistorico::class){
                    switch ($status) {
                        case 'INCPEN':
                            ($envia_pncp->tipo_contrato == 'Contrato') ? 
                            $this->insereContrato($envia_pncp) : $this->insereTermoContrato($envia_pncp);
                            break;
                        case 'RETPEN':
                            ($envia_pncp->tipo_contrato == 'Contrato') ? 
                            $this->atualizaContrato($envia_pncp) : $this->atualizaTermoContrato($envia_pncp);
                            break;
                        case 'ARQPEN':
                            $this->enviaContratoArquivos($envia_pncp);
                            break;
                        case 'EXCPEN':
                            ($envia_pncp->tipo_contrato == 'Contrato') ? 
                            $this->excluiContrato($envia_pncp) : $this->excluiTermoContrato($envia_pncp);
                            break;

                        default: return;
                    }
                }else{
                    switch ($status) {
                        case 'INCPEN':
                            $this->insereEmpenho($envia_pncp);
                            break;
                        case 'ARQPEN':
                            $this->enviaEmpenhoArquivo($envia_pncp);
                            break;
                        /*case 'RETPEN':
                            $this->atualizaContrato($envia_pncp);
                            break;
                        case 'INCARC':
                            $this->insereContratoArquivo($envia_pncp);
                            break;
                        case 'ATUARC':
                            $this->atualizaContratoArquivo($envia_pncp);
                            break;
                        case 'DELARC':
                            $this->deletaContratoArquivo($envia_pncp);
                            break;*/
                        default: return;
                    }
                }
            }catch(Exception $e){
                Log::error($e);
            }
        }else{
            //\Alert::error('Status Inválido para envio unitário')->flash();
        }
        return redirect('admin/pncp');
    }

    public function loginPNCP(){
        
        $uc = new UsuarioController();
        $uc->login();
        return redirect('admin/pncp');
    }

    public function preencheLinksArquivoEmpenho(){
        Log::info("Sincronizando arquivos");
        DB::beginTransaction();
        try{
            $cc = new ContratoControllerPNCP();
            $situacao  = Codigoitem::whereHas('codigo', function ($q) {
                $q->where('descricao', 'Situação Envia Dados PNCP');
            })->where('descres', 'SUCESSO')->first()->id;
            $situacaoARQ  = Codigoitem::whereHas('codigo', function ($q) {
                $q->where('descricao', 'Situação Envia Dados PNCP');
            })->where('descres', 'ARQPEN')->first()->id;
    
            DB::table('envia_dados_pncp')->where([
                ['pncpable_type', MinutaEmpenho::class],
                ['linkArquivoEmpenho', null],
                ['link_pncp','<>', null],
                ])
            ->chunkById(100, function ($envia) use ($cc, $situacao, $situacaoARQ) {
                foreach ($envia as $e) {
                    $linkArquivo = @$cc->consultarContratoUrlMontada($e->link_pncp.'/arquivos')[0]["url"];
                    if($linkArquivo !== null){
                        DB::table('envia_dados_pncp')
                        ->where('id', $e->id)
                        ->update(['linkArquivoEmpenho' => $linkArquivo]);
                        if($e->situacao == $situacaoARQ){
                            DB::table('envia_dados_pncp')
                            ->where('id', $e->id)
                            ->update(['situacao' => $situacao]);
                        }
                    }
                }
            });
        DB::commit();
        }catch(Exception $e){
            Log::error($e);
            DB::rollback();
        }

    
        return redirect('admin/pncp');
    } 


    public function buscaArquivoEmpenhoAssinado(){
        Log::info("buscando Arquivos");
        $me = new MinutaEmpenhoCrudController();
        $me->geraArquivoEmpenhoPNCP();
        return redirect('admin/pncp');
    }    
    public function __construct(){
        //otimizar
        $this->cc = new ContratoControllerPNCP();
        $this->cscc = new CompraSiasgCrudController();
        $this->dcc = new DocumentoContratoController();
        $this->dtcc = new DocumentoTermoContratoController();
        $this->tcc = new TermoContratoController();
    }

    public function insereContratos(){

        $envia_contratos_pncp = EnviaDadosPncp::whereHas('status', function ($ci) {
            $ci->where('descres','INCPEN');
        })->where('pncpable_type', Contratohistorico::class)->get();

        foreach ($envia_contratos_pncp as $contrato_pncp){
            try{
                ($contrato_pncp->tipo_contrato == 'Contrato') ? 
                $this->insereContrato($contrato_pncp) : $this->insereTermoContrato($contrato_pncp);
            }catch(Exception $e){
                Log::error($e);
            }
        }

    }

    public function insereEmpenhos(){

        $envia_contratos_pncp = EnviaDadosPncp::whereHas('status', function ($ci) {
            $ci->where('descres','INCPEN');
        })->where('pncpable_type', MinutaEmpenho::class)->get();
        
        foreach ($envia_contratos_pncp as $contrato_pncp){
            try{
                $this->insereEmpenho($contrato_pncp);
            }catch(Exception $e){
                Log::error($e);
            }
        }

    }

    public function atualizaContratos(){

        $envia_contratos_pncp = EnviaDadosPncp::whereHas('status', function ($ci) {
            $ci->where('descres','RETPEN');
        })->where('pncpable_type', Contratohistorico::class)->get();

        foreach ($envia_contratos_pncp as $contrato_pncp){
            try{
                ($contrato_pncp->tipo_contrato == 'Contrato') ? 
                $this->atualizaContrato($contrato_pncp) : $this->atualizaTermoContrato($contrato_pncp);
            }catch(Exception $e){
                Log::error($e);
            }
        }

    }

    public function excluirContratos(){

        $envia_contratos_pncp = EnviaDadosPncp::whereHas('status', function ($ci) {
            $ci->where('descres','EXCPEN');
        })->where('pncpable_type', Contratohistorico::class)->get();

        foreach ($envia_contratos_pncp as $contrato_pncp){
            try{
                ($contrato_pncp->tipo_contrato == 'Contrato') ? 
                $this->excluiContrato($contrato_pncp) : $this->excluiTermoContrato($contrato_pncp);
            }catch(Exception $e){
                Log::error($e);
            }
        }

    }

    public function enviaArquivos(){

        $envia_arquivos_pncp = EnviaDadosPncp::whereHas('status', function ($ci) {
            $ci->where('descres','ARQPEN');
        })->get();
        
        foreach ($envia_arquivos_pncp as $envia_pncp){
            try{
                if($envia_pncp->pncpable_type == Contratohistorico::class){
                    $this->enviaContratoArquivos($envia_pncp);
                }else if($envia_pncp->pncpable_type == MinutaEmpenho::class){
                    $this->enviaEmpenhoArquivo($envia_pncp);
                }else{
                    //situacao futura
                }
            }catch(Exception $e){
                Log::error($e);
            }
        }
    }

    public function atualizaEmpenhos(){

        $envia_contratos_pncp = EnviaDadosPncp::whereHas('status', function ($ci) {
            $ci->where('descres','RETPEN');
        })->where('pncpable_type', MinutaEmpenho::class)->get();

        foreach ($envia_contratos_pncp as $contrato_pncp){
            try{
                $this->atualizaEmpenho($contrato_pncp);
            }catch(Exception $e){
                Log::error($e);
            }
        }

    }

    public function insereContrato($contrato_pncp){
        try{
        $contrato = Contratohistorico::find($contrato_pncp->pncpable_id);
        $contratoEnviado = null;
        $sispp = $this->buscaCNPJContratante($contrato->modalidade->descres, 
        $contrato->unidadecompra->codigo, 
        $contrato->unidade->codigo, 
        str_replace('/', '',$contrato->licitacao_numero));
        
        $cnpjOrgao = $this->cnpjOrgao($contrato);
        $contratoEnviado = $this->cc->inserirContrato($contrato, $sispp->cnpjOrgao, $sispp->idUnico, $cnpjOrgao);

        $enviaArquivos = $this->trataRetorno($contrato_pncp, $contratoEnviado, false, 'numeroControlePNCP');
        if($enviaArquivos){
            $this->enviaContratoArquivos(EnviaDadosPncp::find($contrato_pncp->id));
        }
        }catch(Exception $e){
            $this->registraErro($contrato_pncp, false, $e);
            throw $e;
        }

    }

    public function insereEmpenho($contrato_pncp){
        try{
        $empenho = MinutaEmpenho::find($contrato_pncp->pncpable_id);
        $empenhoEnviado = null;
        $sispp = $this->buscaCNPJContratante($empenho->compra->modalidade->descres
        ,$empenho->compra->unidade_origem->codigo
        ,$empenho->unidade_id()->first()->codigo
        ,str_replace('/', '',$empenho->compra->numero_ano));
        
        //$cnpjOrgao = $empenho->unidade_id()->first()->orgao->cnpj;
        $cnpjOrgao = $empenho->saldo_contabil->unidade_id()->first()->orgao->cnpj;

        $empenhoEnviado = $this->cc->inserirContrato($empenho, $sispp->cnpjOrgao, $sispp->idUnico, $cnpjOrgao);
        
        $this->trataRetorno($contrato_pncp, $empenhoEnviado, false, 'numeroControlePNCP');
        if(file_exists($empenho->serializaArquivoPNCP())){
            $this->enviaEmpenhoArquivo(EnviaDadosPncp::find($contrato_pncp->id));
        }
        }catch(Exception $e){
            $this->registraErro($contrato_pncp, false, $e);
            throw $e;
        }

    }

    public function atualizaContrato($contrato_pncp){
        try{
        $contrato = Contratohistorico::find($contrato_pncp->pncpable_id);
        $contratoEnviado = null;
        $sispp = $this->buscaCNPJContratante($contrato->modalidade->descres, 
        $contrato->unidadecompra->codigo, 
        $contrato->unidade->codigo, 
        str_replace('/', '',$contrato->licitacao_numero));
        
        $cnpjOrgao = $this->cnpjOrgao($contrato);
        $ano = explode('/', $contrato->licitacao_numero)[1];
        $sequencialPNCP = explode('-', explode('/', $contrato_pncp->sequencialPNCP)[0])[2];
        $contratoEnviado = $this->cc->retificarContrato($contrato, $sispp->cnpjOrgao, $sispp->idUnico, $ano, $cnpjOrgao, $sequencialPNCP);

        $enviaArquivos = $this->trataRetorno($contrato_pncp, $contratoEnviado, true, '');
        if($enviaArquivos){
            $this->enviaContratoArquivos(EnviaDadosPncp::find($contrato_pncp->id));
        }
        }catch(Exception $e){
            $this->registraErro($contrato_pncp, true, $e);
            throw $e;
        }
    }

    //remover comentários quando o Comprasnet habilitar o softdeletes para os Contratos.
    public function excluiContrato($contrato_pncp){
        try{
        //$contratoExcluido = Contratohistorico::withTrashed()->find($contrato_pncp->pncpable_id);
        $envia_pncp = EnviaDadosPncp::find($contrato_pncp->id);
        $contratoExcluidoPNCP = null;
        
        //$cnpjOrgao = $contratoExcluido->unidade->orgao->cnpj;
        $cnpjOrgao = explode('-', explode('/', $envia_pncp->sequencialPNCP)[0])[0];
        //$ano = explode('/', $contratoExcluido->licitacao_numero)[1];
        $ano = explode('/', $envia_pncp->sequencialPNCP)[1];
        $sequencialPNCP = explode('-', explode('/', $envia_pncp->sequencialPNCP)[0])[2];
        //$contratoExcluidoPNCP = $this->cc->excluirContrato($contratoExcluido, $cnpjOrgao, $ano, $sequencialPNCP);
        $contratoExcluidoPNCP = $this->cc->excluirContrato($cnpjOrgao, $ano, $sequencialPNCP);
        $this->trataRetornoExclusao($envia_pncp, $contratoExcluidoPNCP);
        }catch(Exception $e){
            $this->registraErro($envia_pncp, true, $e);
            throw $e;
        }
    }
    
    public function insereTermoContrato($contrato_pncp){
        try{
        $termo = Contratohistorico::find($contrato_pncp->pncpable_id);
        $enviaPNCPBase = EnviaDadosPncp::where([
            ['contrato_id', '=', $termo->contrato_id],
            ['tipo_contrato', '=', 'Contrato'],
        ])->oldest()->first();
        $termoEnviado = null;

        $cnpjOrgao = explode('-', explode('/', $enviaPNCPBase->sequencialPNCP)[0])[0];
        $ano = explode('/', $enviaPNCPBase->sequencialPNCP)[1];
        $sequencialPNCP = explode('-', explode('/', $enviaPNCPBase->sequencialPNCP)[0])[2];
        $termoEnviado = $this->tcc->inserirTermoContrato($termo, $cnpjOrgao, $ano, $sequencialPNCP);
        //dd($termoEnviado);
        $enviaArquivos = $this->trataRetorno($contrato_pncp, $termoEnviado, false, 'sequencialTermoContrato');
        if($enviaArquivos){
            $this->enviaContratoArquivos(EnviaDadosPncp::find($termo->id));
        }
        }catch(Exception $e){
            $this->registraErro($contrato_pncp, false, $e);
            throw $e;
        }

    }

    public function atualizaTermoContrato($contrato_pncp){
        try{
        $termo = Contratohistorico::find($contrato_pncp->pncpable_id);
        $enviaPNCPBase = EnviaDadosPncp::where([
            ['contrato_id', '=', $termo->contrato_id],
            ['tipo_contrato', '=', 'Contrato'],
        ])->oldest()->first();
        $termoRetificado = null;

        $cnpjOrgao = explode('-', explode('/', $enviaPNCPBase->sequencialPNCP)[0])[0];
        $ano = explode('/', $enviaPNCPBase->sequencialPNCP)[1];
        $sequencialPNCP = explode('-', explode('/', $enviaPNCPBase->sequencialPNCP)[0])[2];
        $termoRetificado = $this->tcc->retificarTermoContrato($termo, $cnpjOrgao, $ano, $sequencialPNCP, $contrato_pncp->sequencialPNCP);

        $enviaArquivos = $this->trataRetorno($contrato_pncp, $termoRetificado, true, '');
        if($enviaArquivos){
            $this->enviaContratoArquivos(EnviaDadosPncp::find($contrato_pncp->id));
        }
        }catch(Exception $e){
            $this->registraErro($contrato_pncp, true, $e);
            throw $e;
        }

    }

    public function excluiTermoContrato($contrato_pncp){
        try{
        $envia_pncp = EnviaDadosPncp::find($contrato_pncp->id);
        $enviaPNCPBase = EnviaDadosPncp::where([
            ['contrato_id', '=', $envia_pncp->contrato_id],
            ['tipo_contrato', '=', 'Contrato'],
        ])->oldest()->first();
        $contratoExcluidoPNCP = null;
        
        $cnpjOrgao = explode('-', explode('/', $enviaPNCPBase->sequencialPNCP)[0])[0];
        $ano = explode('/', $enviaPNCPBase->sequencialPNCP)[1];
        $sequencialPNCP = explode('-', explode('/', $enviaPNCPBase->sequencialPNCP)[0])[2];
        $contratoExcluidoPNCP = $this->tcc->excluirTermoContrato($cnpjOrgao, $ano, $sequencialPNCP, $contrato_pncp->sequencialPNCP);
        $this->trataRetornoExclusao($envia_pncp, $contratoExcluidoPNCP);
        }catch(Exception $e){
            $this->registraErro($envia_pncp, true, $e);
            throw $e;
        }

    }

    public function enviaContratoArquivos($envia_pncp){
        $contrato = Contratohistorico::where('contrato_id','=',$envia_pncp->contrato_id)->first();
        $arquivos = Contratoarquivo::withTrashed()->where('contratohistorico_id',$envia_pncp->pncpable_id)
        ->whereNotNull('envio_pncp_pendente')->get();
        foreach ($arquivos as $arquivo){
            try{
                $documentoEnviado = null;
                $cnpjOrgao = $this->cnpjOrgao($contrato);
                $ano = explode('/', $contrato->licitacao_numero)[1];
                $sequencialPNCP = null;
                if($envia_pncp->tipo_contrato == 'Contrato'){
                    $sequencialPNCP = explode('-', explode('/', $envia_pncp->sequencialPNCP)[0])[2];
                }else{
                    $envia_pncp_base = EnviaDadosPncp::where([
                        ['contrato_id', '=', $envia_pncp->contrato_id],
                        ['tipo_contrato', '=', 'Contrato'],
                    ])->oldest()->first();
                    $sequencialPNCP = explode('-', explode('/', $envia_pncp_base->sequencialPNCP)[0])[2];
                    $sequencialTermoPNCP = $envia_pncp->sequencialPNCP;
                }
                $tipoEnvio = $arquivo->envio_pncp_pendente;

                switch ($tipoEnvio){
                    case 'INCARQ':
                        $arquivo->getTipo() == 'Contrato'?
                        $documentoEnviado = $this->dcc->inserirDocumentoContrato($arquivo, $cnpjOrgao, $ano, $sequencialPNCP)
                        : $documentoEnviado = $this->dtcc->inserirDocumentoTermoContrato($arquivo, $cnpjOrgao, $ano, $sequencialPNCP, $sequencialTermoPNCP);
                        break;
                    case 'ATUARQ':
                        try{
                            if($arquivo->getTipo() == 'Contrato'){
                                $exclusao = $this->dcc->excluirDocumentoContrato($arquivo,$cnpjOrgao, $ano, $sequencialPNCP,$arquivo->sequencial_pncp);
                                $documentoEnviado = $this->dcc->inserirDocumentoContrato($arquivo, $cnpjOrgao, $ano, $sequencialPNCP);
                            }else{
                                $exclusao = $this->dtcc->excluirDocumentoTermoContrato($arquivo,$cnpjOrgao, $ano, $sequencialPNCP,$sequencialTermoPNCP, $arquivo->sequencial_pncp);
                                $documentoEnviado = $this->dtcc->inserirDocumentoTermoContrato($arquivo, $cnpjOrgao, $ano, $sequencialPNCP, $sequencialTermoPNCP);
                            }
                        }catch(Exception $f){
                            //ver qual é o retorno da exclusão
                            if(isset($exclusao)){
                                if($exclusao->status == "201" || $exclusao->status == "200"){
                                    $this->atualizaSituacaoArquivo($envia_pncp,"INCARQ");
                                }
                            }
                            throw $f;
                        }
                        break;
                    case 'DELARQ':
                        //$sequencialDocumento = explode('-', explode('/', $arquivo->sequencial_pncp)[0])[2];
                        $arquivo->getTipo() == 'Contrato'?
                        $documentoEnviado = $this->dcc->excluirDocumentoContrato($arquivo, $cnpjOrgao, $ano, $sequencialPNCP,$arquivo->sequencial_pncp):
                        $documentoEnviado = $this->dtcc->excluirDocumentoTermoContrato($arquivo,$cnpjOrgao, $ano, $sequencialPNCP,$sequencialTermoPNCP, $arquivo->sequencial_pncp);
                        break;

                    default: break;
                }

                $this->trataRetornoArquivo($envia_pncp, $documentoEnviado, $tipoEnvio == 'DELARQ'? true : false, $arquivo, false);
            }catch(Exception $e){
                Log::error($e);
                $this->registraErro($envia_pncp, false, $e);
                $this->registraErroArquivo($e,$arquivo);
            }
        }
    }

    public function enviaEmpenhoArquivo($envia_pncp){
        $empenho = MinutaEmpenho::find($envia_pncp->pncpable_id);
        try{
            $documentoEnviado = null;
            $cnpjOrgao = explode('-', explode('/', $envia_pncp->sequencialPNCP)[0])[0];
            $ano = explode('/', $empenho->compra->numero_ano)[1];
            $sequencialPNCP = explode('-', explode('/', $envia_pncp->sequencialPNCP)[0])[2];
            $documentoEnviado = $this->dcc->inserirDocumentoContrato($empenho, $cnpjOrgao, $ano, $sequencialPNCP);
            $this->trataRetornoArquivo($envia_pncp, $documentoEnviado, true, null, true);
        }catch(Exception $e){
            Log::error($e);
            $this->registraErro($envia_pncp, true, $e);
        }
    }

    private function buscaCNPJContratante($modalidade,$uasgCompra, $uasgUsuario, $numeroAno){
        try{
        
        
        //$request = $this->cscc->consultaCompraSiasgPNCP("06", "201013", "201013", "000992021");
        $request = $this->cscc->consultaCompraSiasgPNCP($modalidade, $uasgCompra ,$uasgUsuario, $numeroAno);

        if($request != null){
            if($request->codigoRetorno == '200' && $request->messagem == 'Sucesso'){
                return $request->data->compraSispp;
            }else{
                throw new Exception($request->messagem." (SIASG)");
            }
        }else{
            throw new Exception("Erro de comunicação com o SIASG (modalidade: ".$modalidade.", uasgCompra: ".$uasgCompra.", uasgUsuario: ".$uasgUsuario.", numeroAno: ".$numeroAno.")");
        }
        
        }catch(Exception $e){
            throw $e;
        }
    }

    private function atualizaSituacaoArquivo($arquivo, $situacao){
        $arquivo = Contratoarquivo::withTrashed()->find($arquivo->id);
        $arquivo->envia_pncp_pendente = $situacao;
        $arquivo->saveWithoutEvents();
    }

    private function trataRetorno($contrato_pncp, $envio, $alteracao, $campoSequencial){
        DB::beginTransaction();
        try{
            $pncpControle = EnviaDadosPncp::find($contrato_pncp->id);

            //PNCP diz, no swagger, que o retorno é 200, mas a API retorna um 201, aguardar definição
            if($envio->getStatusCode() == "201" || $envio->getStatusCode() == "200"){
                $status = "SUCESSO";

                if($alteracao){
                    $pncpControle->json_enviado_alteracao = $envio->JSONEnviado;
                }else{
                    $pncpControle->json_enviado_inclusao = $envio->JSONEnviado;
                    $pncpControle->link_pncp = $envio->getHeader('location')[0];
                    $pncpControle->sequencialPNCP = $this->buscaSequencial($pncpControle->link_pncp, $campoSequencial);
                }
                $enviaArquivos = false;
                if($this->verificaEnvioDeArquivosPendentes($pncpControle->contrato_id ,$pncpControle->pncpable_type, $pncpControle->pncpable_id)){
                    $status = 'ARQPEN';
                    $enviaArquivos = true;
                }
                $pncpControle->situacao = Codigoitem::whereHas('codigo', function ($q) {
                    $q->where('descricao', 'Situação Envia Dados PNCP');
                })->where('descres', $status)->first()->id;
                if($status == "SUCESSO"){
                    $pncpControle->retorno_pncp = "";
                }

            }else{
                //$status = "ERRO";
            }

            $pncpControle->save();
            DB::commit();
            return $enviaArquivos;
        }catch(Exception $e){
            Log::error($e);
            DB::rollback();
        }
    }
    private function cnpjOrgao($contratoH){
        return (@$contratoH->unidadeorigem->codigo != null && $contratoH->unidade->codigo != $contratoH->unidadeorigem->codigo) ? 
        $contratoH->unidadeorigem->orgao->cnpj : $contratoH->unidade->orgao->cnpj;
    }
    private function trataRetornoExclusao($contrato_pncp, $envio){
        DB::beginTransaction();
        try{
            $pncpControle = EnviaDadosPncp::find($contrato_pncp->id);

            //PNCP diz, no swagger, que o retorno é 200, mas a API retorna um 201, aguardar definição
            if($envio->getStatusCode() == "201" || $envio->getStatusCode() == "200"){
                $pncpControle->situacao = Codigoitem::whereHas('codigo', function ($q) {
                    $q->where('descricao', 'Situação Envia Dados PNCP');
                })->where('descres', 'EXCLUIDO')->first()->id;
                $pncpControle->retorno_pncp = "";
            }else{
                //$status = "ERRO";
            }
            $pncpControle->save();
            DB::commit();
        }catch(Exception $e){
            Log::error($e);
            DB::rollback();
        }
    }

    private function verificaEnvioDeArquivosPendentes($id,$tipo,$historico_id){
        
        if($tipo == Contratohistorico::class){
            return Contratoarquivo::withTrashed()->where([
                ['contrato_id', '=',$id],
                ['contratohistorico_id', '=',$historico_id]
            ])->whereNotNull('envio_pncp_pendente')->count() > 0? true:false;
        }
        
    }


    private function buscaSequencial($url, $campo){
        return $this->cc->consultarContratoUrlMontada($url)[$campo];
    }

    private function trataRetornoArquivo($pncpControle, $envio, $remocao, $arquivo, $empenho){
        DB::beginTransaction();
        try{
            //PNCP diz, no swagger, que o retorno é 200, mas a API retorna um 201, aguardar definição
            if($envio->getStatusCode() == "201" || $envio->getStatusCode() == "200"){
                $status = "SUCESSO";

                if(!$remocao){
                    $arquivo->link_pncp = $envio->getHeader('location')[0];
                    //$arquivo->sequencial_pncp = $this->buscaSequencial($arquivo->link_pncp);
                    //gato para pegar a ultima barra do link
                    $link = explode('/', $arquivo->link_pncp);
                    $arquivo->sequencial_pncp = $link[count($link)-1];
                }
                if($arquivo){
                    $arquivo->envio_pncp_pendente = null;
                }
                if($empenho){
                    $pncpControle->linkArquivoEmpenho = $envio->getHeader('location')[0];
                }
                $pncpControle->situacao = Codigoitem::whereHas('codigo', function ($q) {
                    $q->where('descricao', 'Situação Envia Dados PNCP');
                })->where('descres', $status)->first()->id;
                if($status == "SUCESSO"){
                    $pncpControle->retorno_pncp = "";
                }

            }else{
                //$status = "ERRO";
            }

            $pncpControle->save();
            if($arquivo){
                $arquivo->saveWithoutEvents();
            }
            DB::commit();
        }catch(Exception $e){
            Log::error($e);
            DB::rollback();
        }
    }
    private function registraErro($contrato_pncp, $alteracao,$erro){
        DB::beginTransaction();
        try{
            $pncpControle = EnviaDadosPncp::find($contrato_pncp->id);
            
            if(isset($erro->body)){
                if($alteracao){
                    $pncpControle->json_enviado_alteracao = $erro->body;
                }else{
                    $pncpControle->json_enviado_inclusao = $erro->body;
                }
            }

            $pncpControle->retorno_pncp = $erro->getMessage();

            $pncpControle->save();
            DB::commit();
        }catch(Exception $e){
            Log::error($e);
            DB::rollback();
        }
    }

    private function registraErroArquivo($erro, $arquivo){
        DB::beginTransaction();
        try{
            $arquivo = EnviaDadosPncp::withTrashed()->find($arquivo->id);
            $arquivo->retorno_pncp = $erro->getMessage();
            $arquivo->saveWithoutEvents();
            DB::commit();
        }catch(Exception $e){
            Log::error($e);
            DB::rollback();
        }
    }

}

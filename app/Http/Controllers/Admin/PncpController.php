<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\BuscaCodigoItens;
use Backpack\CRUD\app\Http\Controllers\CrudController;

class PncpController extends CrudController
{
    use BuscaCodigoItens;

    public function setup()
    {
        if (!backpack_user()->hasRole('Administrador')) {
            abort('403', config('app.erro_permissao'));
        }

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
//        $this->crud->setModel(ContratoPublicacoes::class);
        $this->crud->setModel('App\Models\EnviaDadosPncp');
        $this->crud->setRoute(config('backpack.base.route_prefix')
            . "/admin/pncp");
        $this->crud->setEntityNameStrings('PNCP', 'PNCP');
        $this->crud->orderBy('updated_at', 'desc');
        $this->crud->addClause('select', 'envia_dados_pncp.*');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */
        $this->crud->enableExportButtons();
        $this->crud->addButtonFromView('line', 'envia_individual_pncp', 'envia_individual_pncp', 'beginning');
        $this->crud->allowAccess('delete');
        $this->crud->allowAccess('show');
        $this->crud->allowAccess('update');
        $this->crud->denyAccess('create');

        // TODO: remove setFromDb() and manually define Fields and Columns
//        $this->crud->setFromDb();

        $this->crud->addButtonFromView('top', 'voltar', 'voltarcontrato', 'end');
        (backpack_user()->hasRole('Administrador')) ? $this->crud->addButtonFromView('top', 'Login','login_pncp', 'end') : null;
        (backpack_user()->hasRole('Administrador')) ? $this->crud->addButtonFromView('top', 'Preencher Links','busca_arquivo_empenho_pncp', 'end') : null;
        (backpack_user()->hasRole('Administrador')) ? $this->crud->addButtonFromView('top', 'Gerar Arquivos de Empenhos','gera_arquivos_empenhos', 'end') : null;
        (backpack_user()->hasRole('Administrador')) ? $this->crud->addButtonFromView('top', 'Enviar todos','envia_pncp', 'end') : null;
        //TODO SUPRIMIR O BTN DE ATUALIZAR SITUAÇÃO ATÉ SEGUNDA ORDEM
//        $this->crud->addButtonFromView('line', 'atualizarsituacaopublicacao', 'atualizarsituacaopublicacao');
        $this->crud->enableExportButtons();

        $this->adicionaColunas();
        $this->adicionaCampos();

    }

    /**
     * Configura a grid de visualização
     *
     */
    protected function adicionaColunas()
    {
        $this->adicionaColunaId();
        $this->adicionaColunaTipo();
        $this->adicionaColunaTipoContrato();
        $this->adicionaColunaIdEntidade();
        $this->adicionaColunaSituacao();
        $this->adicionaColunaEnviadoI();
        $this->adicionaColunaEnviadoA();
        $this->adicionaColunaRetorno();
        $this->adicionaColunaLink();
        $this->adicionaColunaSequencial();
        $this->adicionaColunaContratoId();
        $this->adicionaColunalinkArquivoEmpenho();
        $this->adicionaUpdatedAt();
    }

    protected function adicionaCampos()
    {
        $this->adicionaCampoSituacao();
        $this->adicionaCampoEnviadoI();
        $this->adicionaCampoEnviadoA();
        $this->adicionaCampoRetorno();
        $this->adicionaCampoLink();
        $this->adicionaCampoSequencial();
    }

    private function adicionaCampoSituacao(): void
    {
        $this->crud->addField([
            'name' => 'status.descres',
            'label' => 'Situação',
            'type' => 'text',
            /*'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ]*/
        ]);
    }
    private function adicionaCampoEnviadoI(): void
    {
        $this->crud->addField([
            'name' => 'json_enviado_inclusao',
            'label' => 'Json Enviado Inclusão',
            'type' => 'text',
            /*'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ]*/
        ]);
    }
    private function adicionaCampoEnviadoA(): void
    {
        $this->crud->addField([
            'name' => 'json_enviado_alteracao',
            'label' => 'Json Enviado Alteração',
            'type' => 'text',
            /*'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ]*/
        ]);
    }
    private function adicionaCampoRetorno(): void
    {
        $this->crud->addField([
            'name' => 'retorno_pncp',
            'label' => 'Retorno',
            'type' => 'text',
            /*'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ]*/
        ]);
    }
    private function adicionaCampoLink(): void
    {
        $this->crud->addField([
            'name' => 'link_pncp',
            'label' => 'Link',
            'type' => 'text',
            /*'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ]*/
        ]);
    }
    private function adicionaCampoSequencial(): void
    {
        $this->crud->addField([
            'name' => 'sequencialPNCP',
            'label' => 'Sequencial',
            'type' => 'text',
            /*'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ]*/
        ]);
    }


    /**
     * Cofigura a coluna
     */
    private function adicionaColunaId(): void
    {
        $this->crud->addColumn([
            'name' => 'id',
            'label' => 'Id',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
        ]);
    }
    /**
     * Cofigura a coluna
     */
    private function adicionaColunaTipo(): void
    {
        $this->crud->addColumn([
            'name' => 'pncpable_type',
            'label' => 'Tipo',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
        ]);
    }

        /**
     * Cofigura a coluna
     */
    private function adicionaColunaTipoContrato(): void
    {
        $this->crud->addColumn([
            'name' => 'tipo_contrato',
            'label' => 'Tipo Contrato',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
        ]);
    }

    /**
     * Cofigura a coluna
     */
    private function adicionaColunaIdEntidade(): void
    {
        $this->crud->addColumn([
            'name' => 'pncpable_id',
            'label' => 'Id Entidade',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
        ]);
    }

    /**
     * Cofigura a coluna
     */
    private function adicionaUpdatedAt(): void
    {
        $this->crud->addColumn([
            'name' => 'updated_at',
            'label' => 'Atualizado em',
            'type' => 'datetime',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
        ]);
    }
    /**
     * Cofigura a coluna
     */
    private function adicionaColunaSituacao(): void
    {
        $this->crud->addColumn([
            'name' => 'status.descres',
            'label' => 'Situação',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
        ]);
    }
    /**
     * Cofigura a coluna
     */
    private function adicionaColunaEnviadoI(): void
    {
        $this->crud->addColumn([
            'name' => 'json_enviado_inclusao',
            'label' => 'Json Enviado Inclusão',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
            //'limit' => 20000,
        ]);
    }

        /**
     * Cofigura a coluna
     */
    private function adicionaColunaEnviadoA(): void
    {
        $this->crud->addColumn([
            'name' => 'json_enviado_alteracao',
            'label' => 'Json Enviado Alteração',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
        ]);
    }

    /**
     * Cofigura a coluna
     */
    private function adicionaColunaRetorno(): void
    {
        $this->crud->addColumn([
            'name' => 'retorno_pncp',
            'label' => 'Retorno',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
        ]);
    }

    /**
     * Cofigura a coluna
     */
    private function adicionaColunaLink(): void
    {
        $this->crud->addColumn([
            'name' => 'link_pncp',
            'label' => 'Link',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
        ]);
    }

        /**
     * Cofigura a coluna
     */
    private function adicionaColunaSequencial(): void
    {
        $this->crud->addColumn([
            'name' => 'sequencialPNCP',
            'label' => 'Sequencial',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
        ]);
    }

            /**
     * Cofigura a coluna
     */
    private function adicionaColunaContratoId(): void
    {
        $this->crud->addColumn([
            'name' => 'contrato_id',
            'label' => 'contrato_id',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
        ]);
    }

                /**
     * Cofigura a coluna
     */
    private function adicionaColunaLinkArquivoEmpenho(): void
    {
        $this->crud->addColumn([
            'name' => 'linkArquivoEmpenho',
            'label' => 'Link Arquivo Empenho',
            'type' => 'text',
            'orderable' => true,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInExport' => true,
            'visibleInShow' => true,
        ]);
    }

}

<?php

namespace App\Http\Controllers;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Facades\Log;

abstract class RestAPIController extends Controller
{   
    protected $client;
    protected $token;
    protected $baseUrl;
    protected $headerPadrao = ['Content-Type' => 'application/json'];

    public function __construct()
    {
        $this->client = new Client(['verify' => false]);   
    }

    abstract function buscaToken();
    abstract function buscaBaseUrl();

    public function post($url, $header, $body){
       try{
        //Log::info($url, $header);
        $response = $this->client->request('POST', $url, [
            'body' => $body,
            'headers' => $header
        ]);
        
        return $response;

        }catch(RequestException $e){
            if(strlen($e->getResponse()->getBody()->getContents())>1){
                $e->getResponse()->getBody()->rewind();
                $erro = new Exception($e->getResponse()->getBody()->getContents().", URL: ".$url);
            }else{
                $erro = new Exception($e->getMessage().", URL: ".$url);
            }
            $erro->status = $e->getResponse()->getStatusCode();
            $erro->body = $body;
            throw $erro;
        }catch(Exception $e){
            $erro = new Exception($e->getMessage().", URL: ".$url);
            $erro->body = $body;
            throw $erro;
        }

    }

    public function postMultipart($url, $header, $paramConteudo, $arquivo){
        try{
         //dd($url, $header, $paramConteudo, $arquivo);
         $response = $this->client->request('POST', $url, [
             'headers' => $header,
             'multipart' => [
                [
                    'name' => $paramConteudo,
                    'contents' => fopen($arquivo, 'r'),
                ]
            ]
         ]);
 
         return $response;
 
        }catch(RequestException $e){
            if(strlen($e->getResponse()->getBody()->getContents())>1){
                $e->getResponse()->getBody()->rewind();
                $erro = new Exception($e->getResponse()->getBody()->getContents().", URL: ".$url);
            }else{
                $erro = new Exception($e->getMessage().", URL: ".$url);
            }
            $erro->status = $e->getResponse()->getStatusCode();
            throw $erro;
        }catch(Exception $e){
            $erro = new Exception($e->getMessage().", URL: ".$url);
            throw $erro;
        }
 
     }

    public function get($url, $header){
        try{
        
            $response = $this->client->request('GET', $url, [
                'headers' => $header
            ]);
            return $response;
    
        }catch(RequestException $e){
            Log::error($e);
            if($e->getResponse() !== null){
                
                if(strlen($e->getResponse()->getBody()->getContents())>1){
                    $e->getResponse()->getBody()->rewind();
                    $erro = new Exception($e->getResponse()->getBody()->getContents().", URL: ".$url);
                }else{
                    $erro = new Exception($e->getMessage().", URL: ".$url);
                }

                $erro->status = $e->getResponse()->getStatusCode();
                throw $erro;
            }
            throw new Exception($e->getMessage().", URL: ".$url);;
        }catch(Exception $e){
            $erro = new Exception($e->getMessage().", URL: ".$url);
            throw $erro;
        }
    }
    public function put($url, $header, $body){
        try{
            
            $response = $this->client->request('PUT', $url, [
                'body' => $body,
                'headers' => $header
            ]);
            return $response;
        }catch(RequestException $e){
            if(strlen($e->getResponse()->getBody()->getContents())>1){
                $e->getResponse()->getBody()->rewind();
                $erro = new Exception($e->getResponse()->getBody()->getContents().", URL: ".$url);
            }else{
                $erro = new Exception($e->getMessage().", URL: ".$url);
            }
            $erro->status = $e->getResponse()->getStatusCode();
            $erro->body = $body;
            throw $erro;
        }catch(Exception $e){
            $erro = new Exception($e->getMessage().", URL: ".$url);
            $erro->body = $body;
            throw $erro;
        }
    }
    public function delete($url, $header, $body){
        try{
            $response = $this->client->request('DELETE', $url, [
                'body' => $body,
                'headers' => $header
            ]);
            return $response;
    
        }catch(RequestException $e){
            if(strlen($e->getResponse()->getBody()->getContents())>1){
                $e->getResponse()->getBody()->rewind();
                $erro = new Exception($e->getResponse()->getBody()->getContents().", URL: ".$url);
            }else{
                $erro = new Exception($e->getMessage().", URL: ".$url);
            }
            $erro->status = $e->getResponse()->getStatusCode();
            throw $erro;
        }catch(Exception $e){
            $erro = new Exception($e->getMessage().", URL: ".$url);
            throw $erro;
        }
    }

    public function montaHeader($autentica, $params){
        if($autentica){
            $params = array_merge($params, ["Authorization" => $this->token]);
        }
        return $params;

    }

    public function montaBody($body){
        return json_encode($body);
    }

    public function montaParametros($url, $params){
        return $url . "?" . http_build_query($params);
    }

    public function montaUrl($url, $params){
        $url = $this->baseUrl.$url;
        if($params){
            $url = $this->montaParametros($url,$params);
        }
        return $url;
    }
    
    public function requestTeste($codigo,$header,$body){
         
         //return new Response($codigo,['location','https://treina.pncp.gov.br/api/pncp/v1/orgaos/00394460000141/contratos/2021/26'],'teste');
         return new Response($codigo,$header,$body);
 
     }

}

<?php
namespace App\Http\Controllers\Gescon;
// inserido
use App\Models\Codigoitem;
use App\Models\Contratoconta;
use App\Models\Contrato;
use App\Models\Unidade;
use Backpack\CRUD\app\Http\Controllers\CrudController;
// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ContratocontaRequest as StoreRequest;
use App\Http\Requests\ContratocontaRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
// inserido
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;
/**
 * Class ContratocontaCrudController
 * @package App\Http\Controllers\Gescon
 * @property-read CrudPanel $crud
 */
class ContratocontaCrudController extends CrudController
{
    public function setup()
    {
        // dd(\Route::current()->getName());
        $contrato_id = \Route::current()->parameter('contrato_id');
        $contrato = Contrato::where('id','=',$contrato_id)
            ->where('unidade_id','=',session()->get('user_ug_id'))->first();
        if(!$contrato){
            abort('403', config('app.erro_permissao'));
        }
        // vamos verificar se a unidade do usuário é sisg ou não. Isso será importante para o tipo de cadastro da conta vinculada
        $unidade = Unidade::where('id','=',session()->get('user_ug_id'))->first();
        $isSisg = $unidade->sisg;
        // array de encargos (fat empresa) - só pode ser 1, 2 ou 3% - array será usado em campos()
        $arrayEncargosFatEmpresa = [1 => '1%', 2 => '2%', 3 => '3%'];
        // array com os códigos bancários
        $arrayCodigosBancarios = Contratoconta::getArrayComCodigosBancarios();
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Contratoconta');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/gescon/contrato/' . $contrato_id . '/contratocontas');
        $this->crud->setEntityNameStrings('Conta-Depósito Vinculada', 'Conta-Depósito Vinculada');

        $this->crud->addButtonFromView('top', 'Sobre', 'sobrecontratoconta', 'begin');
        $this->crud->addButtonFromView('top', 'voltar', 'voltarmeucontrato', 'end');
        $this->crud->addButtonFromView('line', 'morecontratoconta', 'morecontratoconta', 'end');
        $this->crud->addClause('where', 'contrato_id', '=', $contrato_id);
        $this->crud->allowAccess('show');
        // permissões
        (backpack_user()->can('contratoconta_inserir')) ? $this->crud->allowAccess('create') : null;
        (backpack_user()->can('contratoconta_editar')) ? $this->crud->allowAccess('update') : null;
        (backpack_user()->can('contratoconta_deletar')) ? $this->crud->allowAccess('delete') : null;
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */
        $colunas = $this->Colunas($isSisg);
        $campos = $this->Campos($contrato, $arrayEncargosFatEmpresa, $arrayCodigosBancarios, $isSisg);
        $this->crud->addFields($campos);
        // add asterisk for fields that are required in ContratocontaRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }
    public function verificarSeContratoJaPossuiConta($request){
        $contratoId = \Route::current()->parameter('contrato_id');
        if( Contratoconta::where('contrato_id', $contratoId)->count() > 0 ){return true;}
        return false;
    }
    public function getDadosPercentuaisGrupoAESubmodulo22ByEncargoResolucao169Cnj($request){
        // vamos verificar se o usuário informou 1, 2 ou 3% no Encargos e buscar os percentuais de grupo A e submódulo 2.2 relacionados.
        $arrayDadosPercentuais = Contratoconta::getDadosPercentuaisGrupoAESubmodulo22ByEncargoResolucao169Cnj($request);


        // dd($arrayDadosPercentuais);

        // vamos jogar os dados no request, para que sejam salvos
        $percentual_grupo_a_13_ferias = $arrayDadosPercentuais['percentual_grupo_a'];
        $percentual_grupo_a_13_ferias_id = $arrayDadosPercentuais['percentual_grupo_a_id'];
        $request->request->set('percentual_grupo_a_13_ferias', $percentual_grupo_a_13_ferias);
        $request->request->set('percentual_grupo_a_13_ferias_codigoitens_id', $percentual_grupo_a_13_ferias_id);

        $percentual_submodulo_22 = $arrayDadosPercentuais['percentual_submodulo_22'];
        $percentual_submodulo_22_id = $arrayDadosPercentuais['percentual_submodulo_22_id'];
        $request->request->set('percentual_submodulo22', $percentual_submodulo_22);
        $request->request->set('percentual_submodulo_22_id', $percentual_submodulo_22_id);

        $percentual_ferias = $arrayDadosPercentuais['percentual_ferias'];
        $request->request->set('percentual_ferias', $percentual_ferias);

        $percentual_abono_ferias = $arrayDadosPercentuais['percentual_abono_ferias'];
        $request->request->set('percentual_abono_ferias', $percentual_abono_ferias);

        $percentual_13 = $arrayDadosPercentuais['percentual_13'];
        $request->request->set('percentual_13', $percentual_13);

        $percentual_multa_sobre_fgts = $arrayDadosPercentuais['percentual_multa_sobre_fgts'];
        $request->request->set('percentual_multa_sobre_fgts', $percentual_multa_sobre_fgts);


        // dd($request);

        return true;
    }
    public function getDadosPercentuaisGrupoAESubmodulo22ByEncargo($request){
        // vamos verificar se o usuário informou 1, 2 ou 3% no Encargos e buscar os percentuais de grupo A e submódulo 2.2 relacionados.
        $arrayDadosPercentuais = Contratoconta::getDadosPercentuaisGrupoAESubmodulo22ByEncargo($request);
        // vamos jogar os dados no request, para que sejam salvos
        $percentual_grupo_a_13_ferias = $arrayDadosPercentuais['percentual_grupo_a'];
        $percentual_grupo_a_13_ferias_id = $arrayDadosPercentuais['percentual_grupo_a_id'];
        $request->request->set('percentual_grupo_a_13_ferias', $percentual_grupo_a_13_ferias);
        $request->request->set('percentual_grupo_a_13_ferias_codigoitens_id', $percentual_grupo_a_13_ferias_id);
        $percentual_submodulo_22 = $arrayDadosPercentuais['percentual_submodulo_22'];
        $percentual_submodulo_22_id = $arrayDadosPercentuais['percentual_submodulo_22_id'];
        $request->request->set('percentual_submodulo22', $percentual_submodulo_22);
        $request->request->set('percentual_submodulo_22_id', $percentual_submodulo_22_id);
        return true;
    }
    public function alterarVirgulasPorPontos($request){
        $percentual13 = str_replace(',', '.', $request->get('percentual_13'));
        $request->request->set('percentual_13', $percentual13);

        $percentualFerias = str_replace(',', '.', $request->get('percentual_ferias'));
        $request->request->set('percentual_ferias', $percentualFerias);

        $percentualAbonoFerias = str_replace(',', '.', $request->get('percentual_abono_ferias'));
        $request->request->set('percentual_abono_ferias', $percentualAbonoFerias);

        $percentualMultaSobreFgts = str_replace(',', '.', $request->get('percentual_multa_sobre_fgts'));
        $request->request->set('percentual_multa_sobre_fgts', $percentualMultaSobreFgts);

        $percentualSubmodulo22 = str_replace(',', '.', $request->get('percentual_submodulo22'));
        $request->request->set('percentual_submodulo22', $percentualSubmodulo22);
    }
    public function store(StoreRequest $request)
    {
        // vamos alterar as vírgulas por pontos no request
        self::alterarVirgulasPorPontos($request);
        // será permitida apenas uma conta por contrato. Vamos verificar
        if(self::verificarSeContratoJaPossuiConta($request)){
            \Alert::error('Já existe uma Conta-Depósito Vinculada a este contrato!')->flash();
            return redirect()->back();
        }
        // vamos verificar se este banco / agência / conta ainda não foram utilizados
        if(Contratoconta::verificarSeDadosAindaNaoExistemNaBase($request, 'insert')){
            \Alert::error('Estes dados de banco / agência / conta já foram utilizados.')->flash();
            return redirect()->back();
        }
        // A partir de agora, 27/07/2021, o usuário poderá escolher se deseja cadastrar a conta vinculada pelo caderno ou pela resolução. Vamos verificar qual foi a escolha.
        $isContaVinculadaPelaResolucao169Cnj = $request->get('is_conta_vinculada_pela_resolucao169_cnj');
        if($isContaVinculadaPelaResolucao169Cnj == null){
            $isContaVinculadaPelaResolucao169Cnj = false;
            $request->request->set('is_conta_vinculada_pela_resolucao169_cnj', $isContaVinculadaPelaResolucao169Cnj);

            $request->request->set('percentual_13', 0);
            $request->request->set('percentual_ferias', 0);
            $request->request->set('percentual_abono_ferias', 0);
            $request->request->set('percentual_multa_sobre_fgts', 0);
            $request->request->set('percentual_submodulo22', 0);
        }

        if($isContaVinculadaPelaResolucao169Cnj){
            self::getDadosPercentuaisGrupoAESubmodulo22ByEncargoResolucao169Cnj($request);
        } else {
            self::getDadosPercentuaisGrupoAESubmodulo22ByEncargo($request);
        }
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
    public function update(UpdateRequest $request)
    {
        // vamos alterar as vírgulas por pontos no request
        self::alterarVirgulasPorPontos($request);
        // vamos verificar se este banco / agência / conta ainda não foram utilizados
        if(Contratoconta::verificarSeDadosAindaNaoExistemNaBase($request, 'update')){
            \Alert::error('Estes dados de banco / agência / conta já foram utilizados por outra Conta-Depósito Vinculada.')->flash();
            return redirect()->back();
        }
        // A partir de agora, 27/07/2021, o usuário poderá escolher se deseja cadastrar a conta vinculada pelo caderno ou pela resolução. Vamos verificar qual foi a escolha.
        $isContaVinculadaPelaResolucao169Cnj = $request->get('is_conta_vinculada_pela_resolucao169_cnj');


        if($isContaVinculadaPelaResolucao169Cnj == null){
            $isContaVinculadaPelaResolucao169Cnj = false;
            $request->request->set('is_conta_vinculada_pela_resolucao169_cnj', $isContaVinculadaPelaResolucao169Cnj);
            $request->request->set('percentual_13', 0);
            $request->request->set('percentual_ferias', 0);
            $request->request->set('percentual_abono_ferias', 0);
            $request->request->set('percentual_multa_sobre_fgts', 0);
            $request->request->set('percentual_submodulo22', 0);
        }

        if($isContaVinculadaPelaResolucao169Cnj){
            self::getDadosPercentuaisGrupoAESubmodulo22ByEncargoResolucao169Cnj($request);
        } else {
            self::getDadosPercentuaisGrupoAESubmodulo22ByEncargo($request);
        }

        // self::getDadosPercentuaisGrupoAESubmodulo22ByEncargo($request);
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
    /**
     * A partir de agora, 26/07/2021, o usuário poderá informar que deseja cadastrar uma conta vinculada,
     * baseando-se no caderno conta vinculada ou na resolução 169 cnj.
     * Vamos fazer essa verificação, antes de mostrarmos os campos para cadastro.
     */
    public function Campos($contrato, $arrayEncargosFatEmpresa, $arrayCodigosBancarios, $isSisg)
    {
        if($isSisg){$isContaVinculadaPelaResolucao169Cnj = false;} else {$isContaVinculadaPelaResolucao169Cnj = true;}
        if($isContaVinculadaPelaResolucao169Cnj){
            $campos = [
                [   // Hidden
                    'name' => 'is_conta_vinculada_pela_resolucao169_cnj',
                    'type' => 'hidden',
                    'default' => $isContaVinculadaPelaResolucao169Cnj,
                ],
                [   // Hidden
                    'name' => 'contrato_id',
                    'type' => 'hidden',
                    'default' => $contrato->id,
                ],
                [ // select_from_array
                    'name' => 'banco',
                    'label' => "Banco",
                    'type' => 'select2_from_array',
                    'options' => $arrayCodigosBancarios,
                    'allows_null' => false,
                    'allows_multiple' => false, // OPTIONAL; needs you to cast this to array in your model;
                ],
                [
                    'name' => 'agencia',
                    'label' => 'Agência', // Table column heading
                    'type' => 'text',
                    'attributes' => [
                        'maxlength' => '20',
                    ],
                ],
                [
                    'name' => 'conta_corrente',
                    'label' => 'Conta Corrente', // Table column heading
                    'type' => 'text',
                    'attributes' => [
                        'maxlength' => '50',
                    ],
                ],
                [   //
                    'name' => 'percentual_13',
                    'label' => 'Percentual 13º (décimo terceiro) salário',
                    'type' => 'text',
                    // 'type' => 'money_fatura',
                    'prefix' => "%",
                    'attributes' => [
                        // 'maxlength' => '50',
                        'required' => true,
                        'id' => 'percentual_13',
                    ],
                ],
                [   //
                    'name' => 'percentual_ferias',
                    'label' => 'Percentual férias',
                    'type' => 'text',
                    // 'type' => 'money_fatura',
                    'prefix' => "%",
                    'attributes' => [
                        // 'maxlength' => '50',
                        'required' => true,
                        'id' => 'percentual_ferias',
                    ],
                ],
                [   //
                    'name' => 'percentual_abono_ferias',
                    'label' => 'Percentual abono de férias',
                    'type' => 'text',
                    // 'type' => 'money_fatura',
                    'prefix' => "%",
                    'attributes' => [
                        // 'maxlength' => '50',
                        'required' => true,
                        'id' => 'percentual_abono_ferias',
                    ],
                ],
                [   //
                    'name' => 'percentual_multa_sobre_fgts',
                    'label' => 'Percentual multa sobre o FGTS para as rescisões sem justa causa',
                    'type' => 'text',
                    // 'type' => 'money_fatura',
                    'prefix' => "%",
                    'attributes' => [
                        // 'maxlength' => '50',
                        'required' => true,
                        'id' => 'percentual_multa_sobre_fgts',
                    ],
                    // 'default' => number_format($this->percentual_multa_sobre_fgts, 2, ',', '.'),
                ],
                [   //
                    'name' => 'percentual_submodulo22',
                    // 'label' => 'Percentual Submódulo 2.2',
                    'label' => 'Grupo A', // alterado por solicitação da Rosângela em reunião 26/08/21
                    'type' => 'text',
                    // 'type' => 'money_fatura',
                    'prefix' => "%",
                    'attributes' => [
                        // 'maxlength' => '50',
                        'required' => true,
                        'id' => 'percentual_submodulo22',
                    ],
                ],
            ];
        } else {
            $campos = [
                [   // Hidden
                    'name' => 'is_conta_vinculada_pela_resolucao169_cnj',
                    'type' => 'hidden',
                    'default' => $isContaVinculadaPelaResolucao169Cnj,
                ],
                [   // Hidden
                    'name' => 'contrato_id',
                    'type' => 'hidden',
                    'default' => $contrato->id,
                ],
                [ // select_from_array
                    'name' => 'banco',
                    'label' => "Banco",
                    'type' => 'select2_from_array',
                    'options' => $arrayCodigosBancarios,
                    'allows_null' => false,
                    'allows_multiple' => false, // OPTIONAL; needs you to cast this to array in your model;
                    'attributes' => [
                        // 'maxlength' => '50',
                        'required' => true,
                    ],
                ],
                [
                    'name' => 'agencia',
                    'label' => 'Agência', // Table column heading
                    'type' => 'text',
                    'attributes' => [
                        'maxlength' => '20',
                        'required' => true,
                    ],
                ],
                [
                    'name' => 'conta_corrente',
                    'label' => 'Conta Corrente', // Table column heading
                    'type' => 'text',
                    'attributes' => [
                        'maxlength' => '50',
                        'required' => true,
                    ],
                ],
                [
                    'name' => 'fat_empresa',
                    'label' => "Encargo SAT (%)",
                    'type' => 'radio',
                    'options' => $arrayEncargosFatEmpresa,
                    'allows_null' => false,
                    'allows_multiple' => false, // OPTIONAL; needs you to cast this to array in your model;
                    // 'options' => [1 => 'Sim', 0 => 'Não'],
                    'inline' => true,
                    'default' => 1
                ],
            ];
        }
        return $campos;
    }
    public function Colunas($isSisg)
    {
        // não tem busca, pois na listagem aparecerá apenas uma conta vinculada.
        if($isSisg){$isContaVinculadaPelaResolucao169Cnj = false;} else {$isContaVinculadaPelaResolucao169Cnj = true;}
        if($isContaVinculadaPelaResolucao169Cnj){
            $this->crud->addColumn(
                [
                    'name' => 'getTipoContaVinculada',
                    'label' => 'Tipo da Conta', // Table column heading
                    'type' => 'model_function',
                    'function_name' => 'getTipoContaVinculada', // the method in your Model
                    'orderable' => true,
                    'visibleInTable' => true, // no point, since it's a large text
                    'visibleInModal' => true, // would make the modal too big
                    'visibleInExport' => true, // not important enough
                    'visibleInShow' => true, // sure, why not
                ]
            );
            $this->crud->addColumn(
                [
                    'name' => 'banco',
                    'label' => 'Banco',
                    'type' => 'text',
                    'orderable' => true,
                    'visibleInTable' => true, // no point, since it's a large text
                    'visibleInModal' => true, // would make the modal too big
                    'visibleInExport' => true, // not important enough
                    'visibleInShow' => true, // sure, why not
                ],
            );
            $this->crud->addColumn(
                [
                    'name' => 'agencia',
                    'label' => 'Agência',
                    'type' => 'text',
                    'orderable' => true,
                    'visibleInTable' => true, // no point, since it's a large text
                    'visibleInModal' => true, // would make the modal too big
                    'visibleInExport' => true, // not important enough
                    'visibleInShow' => true, // sure, why not
                ],
            );
            $this->crud->addColumn(
                [
                    'name' => 'conta_corrente',
                    'label' => 'Conta Corrente',
                    'type' => 'text',
                    'orderable' => true,
                    'visibleInTable' => true, // no point, since it's a large text
                    'visibleInModal' => true, // would make the modal too big
                    'visibleInExport' => true, // not important enough
                    'visibleInShow' => true, // sure, why not
                ],
            );
            $this->crud->addColumn(
                [
                    'name' => 'percentual_grupo_a_13_ferias',
                    // 'label' => 'Percentual Grupo A',
                    'label' => 'Incidência do Grupo A', // alterado por solicitação da Rosângela após reunião em 26/08/2021
                    'type' => 'number',
                    'decimals' => 2,
                    'orderable' => true,
                    'visibleInTable' => true, // no point, since it's a large text
                    'visibleInModal' => true, // would make the modal too big
                    'visibleInExport' => true, // not important enough
                    'visibleInShow' => true, // sure, why not
                    'prefix' => "% ",
                ],
            );
            $this->crud->addColumn(
                [
                    'name' => 'percentual_submodulo22',
                    // 'label' => 'Percentual Submódulo 2.2',
                    'label' => 'Grupo A',   // alterado por solicitação da Rosângela após reunião em 26/08/2021
                    'type' => 'number',
                    'decimals' => 2,
                    'orderable' => true,
                    'visibleInTable' => true, // no point, since it's a large text
                    'visibleInModal' => true, // would make the modal too big
                    'visibleInExport' => true, // not important enough
                    'visibleInShow' => true, // sure, why not
                    'prefix' => "% ",
                ],
            );
            $this->crud->addColumn(
                [
                    'name' => 'getSaldoContratoContaParaColunas',
                    'label' => 'Saldo da Conta', // Table column heading
                    'type' => 'model_function',
                    'function_name' => 'getSaldoContratoContaParaColunas', // the method in your Model
                    'orderable' => true,
                    'visibleInTable' => true, // no point, since it's a large text
                    'visibleInModal' => true, // would make the modal too big
                    'visibleInExport' => true, // not important enough
                    'visibleInShow' => true, // sure, why not
                    'prefix' => "R$ ",
                ],
            );
            $this->crud->addColumn(
                [
                    'name' => 'getStatusDaConta',
                    'label' => 'Status da Conta', // Table column heading
                    'type' => 'model_function',
                    'function_name' => 'getStatusDaConta', // the method in your Model
                    'orderable' => true,
                    'visibleInTable' => true, // no point, since it's a large text
                    'visibleInModal' => true, // would make the modal too big
                    'visibleInExport' => true, // not important enough
                    'visibleInShow' => true, // sure, why not
                ],
            );
        } else {
            $this->crud->addColumn(
                [
                    'name' => 'getTipoContaVinculada',
                    'label' => 'Tipo da Conta', // Table column heading
                    'type' => 'model_function',
                    'function_name' => 'getTipoContaVinculada', // the method in your Model
                    'orderable' => true,
                    'visibleInTable' => true, // no point, since it's a large text
                    'visibleInModal' => true, // would make the modal too big
                    'visibleInExport' => true, // not important enough
                    'visibleInShow' => true, // sure, why not
                ]
            );
            $this->crud->addColumn(
                [
                    'name' => 'banco',
                    'label' => 'Banco',
                    'type' => 'text',
                    'orderable' => true,
                    'visibleInTable' => true, // no point, since it's a large text
                    'visibleInModal' => true, // would make the modal too big
                    'visibleInExport' => true, // not important enough
                    'visibleInShow' => true, // sure, why not
                ],
            );
            $this->crud->addColumn(
                [
                    'name' => 'agencia',
                    'label' => 'Agência',
                    'type' => 'text',
                    'orderable' => true,
                    'visibleInTable' => true, // no point, since it's a large text
                    'visibleInModal' => true, // would make the modal too big
                    'visibleInExport' => true, // not important enough
                    'visibleInShow' => true, // sure, why not
                ],
            );
            $this->crud->addColumn(
                [
                    'name' => 'conta_corrente',
                    'label' => 'Conta Corrente',
                    'type' => 'text',
                    'orderable' => true,
                    'visibleInTable' => true, // no point, since it's a large text
                    'visibleInModal' => true, // would make the modal too big
                    'visibleInExport' => true, // not important enough
                    'visibleInShow' => true, // sure, why not
                ],
            );
            $this->crud->addColumn(
                [
                    'name' => 'percentual_grupo_a_13_ferias',
                    // 'label' => 'Percentual Grupo A',
                    // 'label' => 'Incidência do Grupo A', // alterado por solicitação da Rosângela após reunião em 26/08/2021
                    'label' => 'Incidência do Submódulo 2.2', // alterado por solicitação da Anne em 08/10/2021
                    'type' => 'number',
                    'decimals' => 2,
                    'orderable' => true,
                    'visibleInTable' => true, // no point, since it's a large text
                    'visibleInModal' => true, // would make the modal too big
                    'visibleInExport' => true, // not important enough
                    'visibleInShow' => true, // sure, why not
                    'prefix' => "% ",
                ],
            );
            $this->crud->addColumn(
                [
                    'name' => 'percentual_submodulo22',
                    // 'label' => 'Percentual Submódulo 2.2',
                    // 'label' => 'Grupo A', // alterado por solicitação da Rosângela em reunião 26/08/21
                    'label' => 'Submódulo 2.2', // alterado por solicitação da Anne em 08/10/21
                    'type' => 'number',
                    'decimals' => 2,
                    'orderable' => true,
                    'visibleInTable' => true, // no point, since it's a large text
                    'visibleInModal' => true, // would make the modal too big
                    'visibleInExport' => true, // not important enough
                    'visibleInShow' => true, // sure, why not
                    'prefix' => "% ",
                ],
            );
            $this->crud->addColumn(
                [
                    'name' => 'getFatEmpresa',
                    'label' => 'Encargo',
                    'type' => 'model_function',
                    'function_name' => 'getFatEmpresa', // the method in your Model
                    'orderable' => true,
                    'visibleInTable' => true, // no point, since it's a large text
                    'visibleInModal' => true, // would make the modal too big
                    'visibleInExport' => true, // not important enough
                    'visibleInShow' => true, // sure, why not
                    // 'prefix' => "% ",
                ],
            );
            $this->crud->addColumn(
                [
                    'name' => 'getSaldoContratoContaParaColunas',
                    'label' => 'Saldo da Conta', // Table column heading
                    'type' => 'model_function',
                    'function_name' => 'getSaldoContratoContaParaColunas', // the method in your Model
                    'orderable' => true,
                    'visibleInTable' => true, // no point, since it's a large text
                    'visibleInModal' => true, // would make the modal too big
                    'visibleInExport' => true, // not important enough
                    'visibleInShow' => true, // sure, why not
                    'prefix' => "R$ ",
                ],
            );
            $this->crud->addColumn(
                [
                    'name' => 'getStatusDaConta',
                    'label' => 'Status da Conta', // Table column heading
                    'type' => 'model_function',
                    'function_name' => 'getStatusDaConta', // the method in your Model
                    'orderable' => true,
                    'visibleInTable' => true, // no point, since it's a large text
                    'visibleInModal' => true, // would make the modal too big
                    'visibleInExport' => true, // not important enough
                    'visibleInShow' => true, // sure, why not
                ],
            );
        }
    }
}

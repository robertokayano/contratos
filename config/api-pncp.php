<?php
return [
    'url' => env('API_PNCP_URL'),
    'tipo_contrato' => [
        '50' => '1',
        '52' => '2',
        '53' => '3',
        '54' => '4',
        '56' => '5',
        '57' => '6',
        '99' => '7',
        '98' => '8',
        '97' => '9',
        '96' => '10',
        '95' => '11',
        '94' => '12'
    ],
        
    'tipo_termo_contrato' => [
        '20' => '1',
        '55' => '2',
        '60' => '3'
    ],
        
    'categoria_contrato' => [
        'Cessão' => '1',
        'Compras' => '2',
        'Informática (TIC)' => '3',
        'Internacional' => '4',
        'Locação Imóveis' => '5',
        'Mão de Obra' => '6',
        'Obras' => '7',
        'Serviços' => '8',
        'Serviços de Engenharia' => '9',
        'Serviços de Saúde' => '10',
        'Empenho Força de Contrato' => '11'
    ],

    'tipo_fornecedor' => [
        'IDGENERICO' => 'PE',
        'FISICA' => 'PF',
        'JURIDICA' => 'PJ',
        'UG' => 'PJ',
    ],

    'amparo_legal' => [
        '138' => '18',
        '139' => '19'
    ],

    'tipo_documento_contrato' => [
        'TR' => '4',
        'CONTRATO' => '12',
        'RESCISAO' => '13',
        'ADITIVO' => '14',
        'APOSTILA' => '15',
    ],

];
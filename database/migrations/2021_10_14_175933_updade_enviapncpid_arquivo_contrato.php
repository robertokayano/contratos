<?php

use App\Models\EnviaDadosPncp;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class UpdadeEnviapncpidArquivoContrato extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        DB::table('envia_dados_pncp')->whereNotNull('contrato_id')
            ->chunkById(100, function ($envia) {
                foreach ($envia as $e) {
                    DB::table('contrato_arquivos')
                        ->where('contrato_id', $e->contrato_id)
                        ->update(['contratohistorico_id' => $e->id]);
                }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('envia_dados_pncp')->whereNotNull('contrato_id')
            ->chunkById(100, function ($envia) {
                foreach ($envia as $e) {
                    DB::table('contrato_arquivos')
                        ->where('contrato_id', $e->contrato_id)
                        ->update(['contratohistorico_id' => null]);
                }
        });
    }
}

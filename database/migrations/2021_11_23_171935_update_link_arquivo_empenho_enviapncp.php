<?php

use App\Http\Controllers\Admin\EnviaPNCPController;
use App\Http\Controllers\Api\PNCP\ContratoControllerPNCP;
use App\Models\Codigoitem;
use App\Models\EnviaDadosPncp;
use App\Models\MinutaEmpenho;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UpdateLinkArquivoEmpenhoEnviapncp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::beginTransaction();
        try{
            $cc = new ContratoControllerPNCP();
            $situacao  = Codigoitem::whereHas('codigo', function ($q) {
                $q->where('descricao', 'Situação Envia Dados PNCP');
            })->where('descres', 'SUCESSO')->first()->id;

            DB::table('envia_dados_pncp')->where([
                ['pncpable_type', MinutaEmpenho::class],
                ['situacao', $situacao],
                ])
            ->chunkById(100, function ($envia) use ($cc) {
                foreach ($envia as $e) {
                    $linkArquivo = @$cc->consultarContratoUrlMontada($e->link_pncp.'/arquivos')[0]["url"];
                    if($linkArquivo !== null){
                        DB::table('envia_dados_pncp')
                        ->where('id', $e->id)
                        ->update(['linkArquivoEmpenho' => $linkArquivo]);
                    }
                }
        });
        DB::commit();
        }catch(Exception $e){
            Log::error($e);
            DB::rollback();
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('envia_dados_pncp')->update([
            'linkArquivoEmpenho' => null,
        ]);
    }
}

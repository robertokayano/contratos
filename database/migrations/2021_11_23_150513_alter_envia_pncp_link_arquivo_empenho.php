<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterEnviaPncpLinkArquivoEmpenho extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('envia_dados_pncp', function (Blueprint $table) {
            $table->string('linkArquivoEmpenho')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('envia_dados_pncp', function (Blueprint $table) {
            $table->dropColumn('linkArquivoEmpenho');
        });
    }
}

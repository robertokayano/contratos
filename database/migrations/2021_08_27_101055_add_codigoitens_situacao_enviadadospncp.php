<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Codigo;
use App\Models\Codigoitem;

class AddCodigoitensSituacaoEnviadadospncp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::create([
            'descricao' => 'Situação Envia Dados PNCP',
            'visivel' => false
        ]);

        Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'INCPEN',
            'descricao' => 'Inclusão Pendente',
            'visivel' => false
        ]);

        Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'ALTPEN',
            'descricao' => 'Alteração Pendente',
            'visivel' => false
        ]);

        Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'ERRO',
            'descricao' => 'Erro',
            'visivel' => false
        ]);

        Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'SUCESSO',
            'descricao' => 'Sucesso',
            'visivel' => false
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Codigo::where([
            'descricao' => 'Situação Envia Dados PNCP',
            'visivel' => false
        ])->forceDelete();
    }
}

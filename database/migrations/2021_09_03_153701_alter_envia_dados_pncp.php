<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterEnviaDadosPncp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('envia_dados_pncp', function (Blueprint $table) {
            $table->integer('contrato_id')->nullable();
            $table->string('sequencialPNCP')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('envia_dados_pncp', function (Blueprint $table) {
            $table->dropColumn('contrato_id');
            $table->dropColumn('sequencialPNCP');
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterColumnCodigoAmparoLegal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('amparo_legal', function (Blueprint $table) {
            $table->string('codigo')->nullable(true)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('amparo_legal', function (Blueprint $table) {
            $table->dropColumn('codigo')->nullable(true)->change();
        });
    }
}

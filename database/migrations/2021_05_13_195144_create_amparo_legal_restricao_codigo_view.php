<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateAmparoLegalRestricaoCodigoView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("create or replace
            view amparo_legal_restricao_codigos as
            select
                u.id::varchar,
                u.nome,
                alr.id as amparo_legal_restricao_id
            from
                unidades u
            inner join amparo_legal_restricoes alr on
                alr.codigo_restricao = u.id::varchar
                and alr.tipo_restricao_id in (202, 203)
            union
            select
                c2.descres::varchar as id,
                c2.descricao as nome,
                alr2.id as amparo_legal_restricao_id
            from
                codigoitens c2
            inner join amparo_legal_restricoes alr2 on
                alr2.codigo_restricao = c2.descres
                and alr2.tipo_restricao_id = 201
            inner join codigos c3 on
                c3.id = c2.codigo_id
            where
                c3.descricao = 'Tipo Administração'
            union
            select
                'S' as id,
                'Sim' as nome,
                alr3.id as amparo_legal_restricao_id
            from
                amparo_legal_restricoes alr3
            where
                alr3.tipo_restricao_id = 204
            union
            select
                n.codigo::varchar as id,
                n.descricao as nome,
                alr4.id as amparo_legal_restricao_id
            from
                amparo_legal_restricoes alr4
            inner join naturezadespesa n on
                n.codigo = alr4.codigo_restricao
            where
                alr4.tipo_restricao_id = 205");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('drop view amparo_legal_restricao_codigos');
    }
}

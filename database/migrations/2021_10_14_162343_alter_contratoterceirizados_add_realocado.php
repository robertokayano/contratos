<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterContratoterceirizadosAddRealocado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contratoterceirizados', function (Blueprint $table) {
            $table->boolean('realocado')->default(false);
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contratoterceirizados', function (Blueprint $table) {
            $table->dropColumn('realocado');
        });
    }
}

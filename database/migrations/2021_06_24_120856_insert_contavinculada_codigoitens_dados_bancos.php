<?php
use App\Models\Codigo;
use App\Models\Codigoitem;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertContavinculadaCodigoitensDadosBancos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Códigos Bancários
        $codigoGrupoA = Codigo::create([
            'descricao' => 'Códigos Bancários',
            'visivel' => true
        ]);

        $codigoitem = Codigoitem::create([
            'codigo_id' => $codigoGrupoA->id,
            'descres' => '001',
            'descricao' => 'Banco do Brasil S.A.'
        ]);

        $codigoitem = Codigoitem::create([
            'codigo_id' => $codigoGrupoA->id,
            'descres' => '104',
            'descricao' => 'CAIXA ECONOMICA FEDERAL'
        ]);

        $codigoitem = Codigoitem::create([
            'codigo_id' => $codigoGrupoA->id,
            'descres' => '184',
            'descricao' => 'Banco Itaú BBA S.A.'
        ]);

        $codigoitem = Codigoitem::create([
            'codigo_id' => $codigoGrupoA->id,
            'descres' => '033',
            'descricao' => 'BANCO SANTANDER (BRASIL) S.A.'
        ]);

        $codigoitem = Codigoitem::create([
            'codigo_id' => $codigoGrupoA->id,
            'descres' => '036',
            'descricao' => 'Banco Bradesco BBI S.A.'
        ]);

        $codigoitem = Codigoitem::create([
            'codigo_id' => $codigoGrupoA->id,
            'descres' => '756',
            'descricao' => 'BANCO COOPERATIVO SICOOB S.A. - BANCO SICOOB'
        ]);



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Codigo::where([
            'descricao' => 'Códigos Bancários',
            'visivel' => true
        ])->forceDelete();
    }
}

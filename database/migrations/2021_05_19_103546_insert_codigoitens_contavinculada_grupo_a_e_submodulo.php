<?php

use App\Models\Codigo;
use App\Models\Codigoitem;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertCodigoitensContavinculadaGrupoAESubmodulo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Grupo A
        $codigoGrupoA = Codigo::create([
            'descricao' => 'Percentual Grupo A',
            'visivel' => false
        ]);
        $codigoitem = Codigoitem::create([
            'codigo_id' => $codigoGrupoA->id,
            'descres' => '7.39',
            'descricao' => '7.39'
        ]);
        $codigoitem = Codigoitem::create([
            'codigo_id' => $codigoGrupoA->id,
            'descres' => '7.60',
            'descricao' => '7.60'
        ]);
        $codigoitem = Codigoitem::create([
            'codigo_id' => $codigoGrupoA->id,
            'descres' => '7.82',
            'descricao' => '7.82'
        ]);

        // Submódulo 2.2
        $codigoDoSubmodulo22 = Codigo::create([
            'descricao' => 'Percentual do Submódulo 2.2',
            'visivel' => false
        ]);
        $codigoitem = Codigoitem::create([
            'codigo_id' => $codigoDoSubmodulo22->id,
            'descres' => '34.8',
            'descricao' => '34.8'
        ]);
        $codigoitem = Codigoitem::create([
            'codigo_id' => $codigoDoSubmodulo22->id,
            'descres' => '35.8',
            'descricao' => '35.8'
        ]);
        $codigoitem = Codigoitem::create([
            'codigo_id' => $codigoDoSubmodulo22->id,
            'descres' => '36.8',
            'descricao' => '36.8'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Codigo::where([
            'descricao' => 'Percentual Grupo A',
            'visivel' => false
        ])->forceDelete();

        Codigo::where([
            'descricao' => 'Percentual do Submódulo 2.2',
            'visivel' => false
        ])->forceDelete();



    }
}

<?php

use App\Models\Codigo;
use App\Models\Codigoitem;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCodigoitensSituacaoExcluiContratoPncp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::where('descricao', 'Situação Envia Dados PNCP')->first();

        Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'EXCPEN',
            'descricao' => 'Exclusão Pendente',
            'visivel' => false
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Codigo::where([
            'descricao' => 'Situação Envia Dados PNCP',
            'visivel' => false
        ])->forceDelete();
    }
}

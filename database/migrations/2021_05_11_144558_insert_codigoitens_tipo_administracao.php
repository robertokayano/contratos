<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Codigo;
use App\Models\Codigoitem;

class InsertCodigoitensTipoAdministracao extends Migration
{
    public function up()
    {

        $codigo = Codigo::create([
            'descricao' => 'Tipo Administração',
            'visivel' => false
        ]);

        Codigoitem::create([
                'codigo_id' => $codigo->id,
                'descres' => "01",
                'descricao' => "Administracao Direta",
                'visivel' => false
            ]);

        Codigoitem::create([
                'codigo_id' => $codigo->id,
                'descres' => "02",
                'descricao' => "Estatal",
                'visivel' => false
            ]);

        Codigoitem::create([
                'codigo_id' => $codigo->id,
                'descres' => "03",
                'descricao' => "Autarquia",
                'visivel' => false
            ]);

        Codigoitem::create([
                'codigo_id' => $codigo->id,
                'descres' => "04",
                'descricao' => "Fundacao",
                'visivel' => false
            ]);

        Codigoitem::create([
                'codigo_id' => $codigo->id,
                'descres' => "05",
                'descricao' => "Empresa Publica Comercial e Financeira",
                'visivel' => false
            ]);

        Codigoitem::create([
                'codigo_id' => $codigo->id,
                'descres' => "06",
                'descricao' => "Economia Mista",
                'visivel' => false
            ]);

        Codigoitem::create([
                'codigo_id' => $codigo->id,
                'descres' => "07",
                'descricao' => "Fundos",
                'visivel' => false
            ]);

        Codigoitem::create([
                'codigo_id' => $codigo->id,
                'descres' => "08",
                'descricao' => "Empresa Publica Industrial e Agricola",
                'visivel' => false
            ]);

        Codigoitem::create([
                'codigo_id' => $codigo->id,
                'descres' => "11",
                'descricao' => "Administracao Direta Estadual",
                'visivel' => false
            ]);

        Codigoitem::create([
                'codigo_id' => $codigo->id,
                'descres' => "12",
                'descricao' => "Administracao Direta Municipal",
                'visivel' => false
            ]);

        Codigoitem::create([
                'codigo_id' => $codigo->id,
                'descres' => "13",
                'descricao' => "Administracao Indireta Estadual",
                'visivel' => false
            ]);

        Codigoitem::create([
                'codigo_id' => $codigo->id,
                'descres' => "14",
                'descricao' => "Administracao Indireta Municipal",
                'visivel' => false
            ]);

        Codigoitem::create([
                'codigo_id' => $codigo->id,
                'descres' => "15",
                'descricao' => "Empresa Privada",
                'visivel' => false
            ]);
    }

    public function down()
    {
        Codigo::where([
            'descricao' => 'Tipo Administração',
            'visivel' => false
        ])->forceDelete();
    }
}

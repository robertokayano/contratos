<button class="btn btn-default ladda-button btn-xs" id="btn-enviar-dados-siafi{{$entry->getKey()}}" data-style="zoom-in"
        title="Enviar para SIAFI">
    <span class="ladda-label"><i class="fa fa-paper-plane"></i></span>
</button>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#btn-enviar-dados-siafi{{$entry->getKey()}}').on('click', function () {
                Swal.fire({
                    title: 'Confirmar envio de dados para SIAFI?',
                    showDenyButton: false,
                    showCancelButton: true,
                    confirmButtonText: `Sim`,
                }).then((result) => {
                    /* Read more about isConfirmed, isDenied below */
                    if (result.isConfirmed) {
                        window.location = "{{url($crud->route) .'/'. $entry->getKey().'/enviarsiafi'}}"
                    }
                })
            });
        });
    </script>

